#!/bin/sh

SCALEDIRS="16x16 22x22 24x24 32x32 48x48"
SUBDIRS="status apps actions"

for scale in $SCALEDIRS; do
	for subdir in $SUBDIRS; do
		cd scalable/$subdir
	if test ! -d ../../$scale/$subdir; then
		mkdir ../../$scale/$subdir;
	fi
		for i in *.svg; do
			inkscape --without-gui --export-png="../../$scale/$subdir/"$( echo $i | cut -d . -f -1 ).png --export-dpi=72 --export-background-opacity=0 --export-width=$( echo $scale | cut -d x -f -1) --export-height=$( echo $scale | cut -d x -f -1) "$i";done
		cd ../../;done
done
