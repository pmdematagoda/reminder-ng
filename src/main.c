/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Reminder-ng: A reminder/to-do application.
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <glib/gi18n.h>

#include "reminder-list.h"
#include "reminder-core.h"
#include "reminder-ui.h"

static void
reminder_main_exit_request_cb (ReminderUI *ui, GMainLoop *main_loop)
{
	g_main_loop_quit (main_loop);
}

static void
reminder_main_activate (GtkApplication *app) {


}

int
main (int argv, char **argc)
{
	GMainLoop *main_loop;
	ReminderCore *reminder_core;
	ReminderUI *reminder_ui;
	GtkApplication *reminder_ng_app;

	gtk_init (&argv, &argc);

	reminder_ng_app = gtk_application_new ("org.pmd.ReminderNG", 0);

	g_signal_connect (reminder_ng_app, "activate", G_CALLBACK (reminder_main_activate), NULL);

	setlocale (LC_ALL, "");
	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	reminder_core = reminder_core_new ();

	gtk_icon_theme_append_search_path (gtk_icon_theme_get_default (),
					   REMINDERNGDATADIR G_DIR_SEPARATOR_S "icons");

	reminder_ui = reminder_ui_new ();
	reminder_ui_attach_core (reminder_ui, reminder_core);

	main_loop = g_main_loop_new (NULL, FALSE);

	g_signal_connect (reminder_ui, "exit-request",
			  G_CALLBACK (reminder_main_exit_request_cb),
			  main_loop);

	g_main_loop_run (main_loop);

	g_object_unref (reminder_core);
	g_object_unref (reminder_ui);
	g_main_loop_unref (main_loop);

	return 0;
}
