/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

typedef enum
{
		NEW_REMINDER_TIME_ERR,
		NEW_REMINDER_VALIDATION_ERR,
		NEW_REMINDER_TITLE_ERR,
		NEW_REMINDER_EPOCH_ERR
} ReminderUIErrorDialog;

typedef enum
{
	DELETE_EXPIRED_REMINDERS,
	PURGE_REMINDERS
} ReminderUIConfirmDialog;

void       reminder_ui_notice_dialog_show_error     (GtkWindow *parent,
						     ReminderUIErrorDialog err_type);

gboolean   reminder_ui_notice_dialog_show_confirm   (GtkWindow *parent,
						     ReminderUIConfirmDialog confirm_type);
