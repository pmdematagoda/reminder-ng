/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib-2.0/glib.h>

#include "reminder-common.h"
#include "reminder-stock-icons.h"

const gchar *
reminder_common_get_priority_icon_name (guint reminder_prio)
{
	const gchar *ret;

	switch (reminder_prio) {

	case 0:
		ret = REMINDER_NG_PRIO_LOW_ICON;
		break;

	case 1:
		ret = REMINDER_NG_PRIO_NORMAL_ICON;
		break;

	case 2:
		ret = REMINDER_NG_PRIO_HIGH_ICON;
		break;

	case 6:
		ret = REMINDER_NG_PRIO_CUSTOM_ICON;
		break;

	default:
		g_warning ("Unknown reminder_prio specified");

		ret = "Unknown";
	}

	return ret;
}
