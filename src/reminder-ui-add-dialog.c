/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <glib-2.0/glib.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>

#include "reminder-list.h"
#include "reminder-stock-icons.h"

#include "reminder-ui-notice-dialog.h"
#include "reminder-ui-add-dialog.h"

#define REMINDER_UI_ADD_DIALOG_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), REMINDER_TYPE_UI_ADD_DIALOG, ReminderUIAddDialogPrivate))

G_DEFINE_TYPE (ReminderUIAddDialog, reminder_ui_add_dialog, G_TYPE_OBJECT)
     struct ReminderUIAddDialogPrivate
     {
	     GtkWidget *dialog;

	     GtkWidget *hbox_title;
	     GtkWidget *title_label;
	     GtkWidget *title_entry;

	     GtkWidget *hbox_time;
	     GtkWidget *time_label;
	     GtkWidget *time_hour_spin;
	     GtkWidget *colon_1_label;
	     GtkWidget *time_minute_spin;
	     GtkWidget *colon_2_label;
	     GtkWidget *time_second_spin;

	     GtkWidget *hbox_date;
	     GtkWidget *date_label;
	     GtkWidget *date_day_spin;
	     GtkWidget *backslash_1_label;
	     GtkWidget *date_month_spin;
	     GtkWidget *backslash_2_label;
	     GtkWidget *date_year_spin;
	     GtkWidget *date_select_expander;
	     GtkWidget *date_calendar;

	     GtkWidget *hbox_prio;
	     GtkWidget *set_prio_label;
	     GtkWidget *set_prio_cmbbox;
	     GtkListStore *set_prio_list;
	     GtkWidget *set_prio_det_button;

	     /* Dialog to set priority details */
	     GtkWidget *prio_det_dialog;

	     GtkWidget *notify_type_title;
	     GtkWidget *notify_off_radio;
	     GtkWidget *notify_notification_radio;
	     GtkWidget *notify_popup_radio;

	     GtkWidget *sound_play_title;
	     GtkWidget *sound_off_radio;
	     GtkWidget *sound_default_radio;

	     GtkWidget *script_path_title;
	     GtkWidget *script_text_box;
	     GtkWidget *script_set_button;

	     GtkWidget *file_chooser_dialog;

	     /* A temporary list of the current list of reminder epochs
	      * so that new reminders to be added can be checked against
	      * them to prevent reminders with the same epochs being
	      * added
	      */
	     GList *current_reminder_list;

	     /* Only used for the add dialog, not for the priority
	      * set dialog
	      */
	     gboolean ok_button_pressed;
     };

/* Possible values for the add_type parameter in the
 * reminder_ui_add_dialog_get_reminder function
 */
enum
{
	QUICK_ADD,
	NORMAL_ADD
};

/* Arrangement of priority levels in the priority combo box */
enum
{
	HIGH_PRIORITY,
	NORMAL_PRIORITY,
	LOW_PRIORITY,
	CUSTOM_PRIORITY
};

static time_t reminder_ui_add_dialog_quick_add (guint quick_add_type);
static void reminder_ui_add_dialog_set_fields (ReminderUIAddDialog *ui_add_dialog,
					       const ReminderList *reminder_list);
static void reminder_ui_add_dialog_get_reminder (ReminderUIAddDialog *ui_add_dialog,
						 ReminderList *new_reminder,
						 guint add_type);
static void reminder_ui_add_dialog_clear_fields (ReminderUIAddDialog *ui_add_dialog);

ReminderList *reminder_ui_add_dialog_quick_add_reminder (ReminderUIAddDialog *ui_add_dialog,
							 guint quick_add_type,
							 GList *current_reminder_list)
{
	time_t new_time;
	struct tm *broken_time;
	ReminderList *new_reminder;

	/* If the add dialog is already shown, just return NULL */
	if (gtk_widget_get_visible (ui_add_dialog->priv->dialog)) {
		return NULL;
	}

	/* Strictly saying, the epoch check should not be necessary here,
	 * but it's better to be safe than sorry, so anyway
	 */
	ui_add_dialog->priv->current_reminder_list = current_reminder_list;

	new_reminder = (ReminderList *) malloc (sizeof (ReminderList));

	new_time =
		time (NULL) +
		reminder_ui_add_dialog_quick_add (quick_add_type);

	broken_time = localtime (&new_time);
	new_reminder->reminder_prio = 1;
	new_reminder->reminder_title = NULL;
	new_reminder->reminder_hour = broken_time->tm_hour;
	new_reminder->reminder_minute = broken_time->tm_min;
	new_reminder->reminder_second = broken_time->tm_sec;
	new_reminder->reminder_day = broken_time->tm_mday;
	new_reminder->reminder_month = broken_time->tm_mon + 1;
	new_reminder->reminder_year = broken_time->tm_year + 1900;
	new_reminder->reminder_epoch = (long int) new_time;

	reminder_ui_add_dialog_set_fields (ui_add_dialog, new_reminder);

	gtk_widget_set_sensitive (ui_add_dialog->priv->time_hour_spin, FALSE);
	gtk_widget_set_sensitive (ui_add_dialog->priv->time_minute_spin,
				  FALSE);
	gtk_widget_set_sensitive (ui_add_dialog->priv->time_second_spin,
				  FALSE);

	gtk_widget_grab_focus (ui_add_dialog->priv->title_entry);

	gtk_widget_show_all (ui_add_dialog->priv->dialog);

	/* Hide the date details as they are irrelevant */
	gtk_widget_hide (ui_add_dialog->priv->hbox_date);
	gtk_widget_hide (ui_add_dialog->priv->date_select_expander);

	/* Disable the button to set priority details as the priority
	 * always selected by default in this case is "Normal"
	 */
	gtk_widget_set_sensitive (ui_add_dialog->priv->set_prio_det_button,
				  FALSE);

	gtk_main ();

	gtk_widget_set_sensitive (ui_add_dialog->priv->time_hour_spin, TRUE);
	gtk_widget_set_sensitive (ui_add_dialog->priv->time_minute_spin,
				  TRUE);
	gtk_widget_set_sensitive (ui_add_dialog->priv->time_second_spin,
				  TRUE);
	gtk_widget_set_sensitive (ui_add_dialog->priv->set_prio_det_button,
				  TRUE);

	if (ui_add_dialog->priv->ok_button_pressed) {
		reminder_ui_add_dialog_get_reminder (ui_add_dialog,
						     new_reminder,
						     QUICK_ADD);

		ui_add_dialog->priv->ok_button_pressed = FALSE;
	} else {
		g_free (new_reminder);
		new_reminder = NULL;
	}
	reminder_ui_add_dialog_clear_fields (ui_add_dialog);

	return new_reminder;
}

ReminderList *
reminder_ui_add_dialog_show (ReminderUIAddDialog *ui_add_dialog,
			     const ReminderList *reminder_list,
			     GList *current_reminder_list)
{
	ReminderList *new_reminder = NULL;

	/* If the add dialog is already shown, just return NULL */
	if (gtk_widget_get_visible (ui_add_dialog->priv->dialog)) {
		return NULL;
	}

	ui_add_dialog->priv->current_reminder_list = current_reminder_list;

	if (reminder_list)
		reminder_ui_add_dialog_set_fields (ui_add_dialog, reminder_list);

	gtk_widget_grab_focus (ui_add_dialog->priv->title_entry);

	gtk_widget_show_all (ui_add_dialog->priv->dialog);

	/* If a reminder is being edited, check if it has a "Custom" priority,
	 * if not, disable the "Set Details" button
	 */
	if (reminder_list) {
		/* If the "Custom" entry has not been selected, disable the button to
		 * set the priority details
		 */
		if (gtk_combo_box_get_active (GTK_COMBO_BOX (ui_add_dialog->priv->set_prio_cmbbox)) == CUSTOM_PRIORITY)
			gtk_widget_set_sensitive (ui_add_dialog->priv->set_prio_det_button,
						  TRUE);
		else
			gtk_widget_set_sensitive (ui_add_dialog->priv->set_prio_det_button,
						  FALSE);
	}
	/* If a new reminder is being entered, just disable the button */
	else
		gtk_widget_set_sensitive (ui_add_dialog->priv->set_prio_det_button,
					  FALSE);

	gtk_main ();

	if (ui_add_dialog->priv->ok_button_pressed) {
		new_reminder = (ReminderList *) g_malloc (sizeof (ReminderList));

		reminder_ui_add_dialog_get_reminder (ui_add_dialog,
						     new_reminder,
						     NORMAL_ADD);

		ui_add_dialog->priv->ok_button_pressed = FALSE;
	}
	/* Clear the data fields regardless of the user's input */
	reminder_ui_add_dialog_clear_fields (ui_add_dialog);

	/* If the expander holding the calendar widget is expanded, collapse it so the next time the
	 * dialog is shown it will look as good as new
	 */
	if (gtk_expander_get_expanded (GTK_EXPANDER (ui_add_dialog->priv->date_select_expander)))
		gtk_expander_set_expanded (GTK_EXPANDER (ui_add_dialog->priv->date_select_expander),
					   FALSE);

	return new_reminder;
}

static time_t
reminder_ui_add_dialog_quick_add (guint quick_add_type)
{
	time_t time_diff = 0;

	switch (quick_add_type)
	  {

	  case FIVE_MINUTE_QUICK_ADD:
		  time_diff = (5 * 60);

		  break;

	  case TEN_MINUTE_QUICK_ADD:
		  time_diff = (10 * 60);

		  break;

	  case FIFTEEN_MINUTE_QUICK_ADD:
		  time_diff = (15 * 60);

		  break;

	  case THIRTY_MINUTE_QUICK_ADD:
		  time_diff = (30 * 60);

		  break;

	  case SIXTY_MINUTE_QUICK_ADD:
		  time_diff = (60 * 60);

		  break;

	  default:
		  g_warning ("A wrong quick_add_type specified");
	  }

	return time_diff;
}

static void
reminder_ui_add_dialog_set_prio_det_fields (ReminderUIAddDialog *ui_add_dialog,
					    ReminderPrioDet *prio_det)
{
	switch (prio_det->vis_alrm_type) {

	case 0:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ui_add_dialog->priv->notify_off_radio),
					      TRUE);
		break;
	case 1:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ui_add_dialog->priv->notify_notification_radio),
					      TRUE);
		break;
	case 2:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ui_add_dialog->priv->notify_popup_radio),
					      TRUE);
		break;

	default:
		g_warning ("Invalid visual notification specified for\n"
			   "priority details");
		return;
	}
	switch (prio_det->snd_alrm_play) {

	case 0:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ui_add_dialog->priv->sound_off_radio),
					      TRUE);
		break;
	case 1:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ui_add_dialog->priv->sound_default_radio),
					      TRUE);
		break;

	default:
		g_warning ("Invalid sound notification specified for\n"
			   "priority details");
		return;
	}

	if (prio_det->script_path) {
		gtk_entry_set_text (GTK_ENTRY (ui_add_dialog->priv->script_text_box),
				    prio_det->script_path);
	}
}

static void
reminder_ui_add_dialog_set_fields (ReminderUIAddDialog *ui_add_dialog,
				   const ReminderList *reminder_list)
{
	switch (reminder_list->reminder_prio) {
	case 0:
		gtk_combo_box_set_active (GTK_COMBO_BOX (ui_add_dialog->priv->set_prio_cmbbox),
					  LOW_PRIORITY);
		break;

	case 1:
		gtk_combo_box_set_active (GTK_COMBO_BOX (ui_add_dialog->priv->set_prio_cmbbox),
					  NORMAL_PRIORITY);
		break;

	case 2:
		gtk_combo_box_set_active (GTK_COMBO_BOX (ui_add_dialog->priv->set_prio_cmbbox),
					  HIGH_PRIORITY);
		break;

		/* Custom priority */
	case 6:
		gtk_combo_box_set_active (GTK_COMBO_BOX (ui_add_dialog->priv->set_prio_cmbbox),
					  CUSTOM_PRIORITY);
		reminder_ui_add_dialog_set_prio_det_fields (ui_add_dialog,
							    reminder_list->prio_det);

		break;
	}

	if (reminder_list->reminder_title)
		gtk_entry_set_text (GTK_ENTRY (ui_add_dialog->priv->title_entry),
				    reminder_list->reminder_title);

	gtk_spin_button_set_value (GTK_SPIN_BUTTON
				   (ui_add_dialog->priv->date_day_spin),
				   reminder_list->reminder_day);

	gtk_spin_button_set_value (GTK_SPIN_BUTTON
				   (ui_add_dialog->priv->date_month_spin),
				   reminder_list->reminder_month);

	gtk_spin_button_set_value (GTK_SPIN_BUTTON
				   (ui_add_dialog->priv->date_year_spin),
				   reminder_list->reminder_year);

	gtk_spin_button_set_value (GTK_SPIN_BUTTON
				   (ui_add_dialog->priv->time_hour_spin),
				   reminder_list->reminder_hour);

	gtk_spin_button_set_value (GTK_SPIN_BUTTON
				   (ui_add_dialog->priv->time_minute_spin),
				   reminder_list->reminder_minute);

	gtk_spin_button_set_value (GTK_SPIN_BUTTON
				   (ui_add_dialog->priv->time_second_spin),
				   reminder_list->reminder_second);

}

static void
reminder_ui_add_dialog_get_prio_ret (ReminderUIAddDialog *ui_add_dialog,
				     ReminderList *new_reminder)
{
	guint vis_alrm;
	gboolean snd_alrm;
	const gchar *script_path;

	/* Visual alarm */
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ui_add_dialog->priv->notify_off_radio)))
	    vis_alrm = 0;
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ui_add_dialog->priv->notify_notification_radio)))
		vis_alrm = 1;
	else
		vis_alrm = 2;

	/* Sound alarm */
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ui_add_dialog->priv->sound_off_radio)))
		snd_alrm = FALSE;
	else
		snd_alrm = TRUE;

	/* Script to execute */
	script_path = gtk_entry_get_text (GTK_ENTRY (ui_add_dialog->priv->script_text_box));
	if (strlen (script_path))
		reminder_list_set_prio_det (new_reminder,
					    vis_alrm,
					    snd_alrm,
					    script_path);
	else
		reminder_list_set_prio_det (new_reminder,
					    vis_alrm,
					    snd_alrm,
					    NULL);
}

static void
reminder_ui_add_dialog_get_reminder (ReminderUIAddDialog *ui_add_dialog,
				     ReminderList *new_reminder,
				     guint add_type)
{
	struct tm new_date;
	guint reminder_prio;

	switch (gtk_combo_box_get_active (GTK_COMBO_BOX (ui_add_dialog->priv->set_prio_cmbbox))) {

		/* High */
	case HIGH_PRIORITY:
		reminder_prio = 2;
		break;

		/* Normal */
	case NORMAL_PRIORITY:
		reminder_prio = 1;
		break;

		/* Low */
	case LOW_PRIORITY:
		reminder_prio = 0;
		break;

		/* Custom */
	case CUSTOM_PRIORITY:
		reminder_prio = 6;
		break;

	default:
		g_warning ("Weird index number of the current selected priority.\n"
			   "Setting priority as \"Normal\"");

		reminder_prio = 1;
	}

	switch (add_type) {

	case NORMAL_ADD:

		new_reminder->reminder_day =
			gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (ui_add_dialog->priv->date_day_spin));
		new_reminder->reminder_month =
			gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (ui_add_dialog->priv->date_month_spin));
		new_reminder->reminder_year =
			gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (ui_add_dialog->priv->date_year_spin));

		new_reminder->reminder_hour =
			gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (ui_add_dialog->priv->time_hour_spin));
		new_reminder->reminder_minute =
			gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (ui_add_dialog->priv->time_minute_spin));
		new_reminder->reminder_second =
			gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (ui_add_dialog->priv->time_second_spin));

		new_date.tm_mday = new_reminder->reminder_day;
		new_date.tm_mon = new_reminder->reminder_month - 1;
		new_date.tm_year = new_reminder->reminder_year - 1900;
		new_date.tm_hour = new_reminder->reminder_hour;
		new_date.tm_min = new_reminder->reminder_minute;
		new_date.tm_sec = new_reminder->reminder_second;

		new_date.tm_isdst = -1;

		new_reminder->reminder_epoch = mktime (&new_date);

	case QUICK_ADD:
		new_reminder->reminder_prio = reminder_prio;
		if (reminder_prio == 6) {
			new_reminder->prio_det = (ReminderPrioDet *) g_malloc (sizeof (ReminderPrioDet));
			reminder_ui_add_dialog_get_prio_ret (ui_add_dialog,
							     new_reminder);
		}

		new_reminder->reminder_tz = -1;

		new_reminder->reminder_title =
			g_strdup (gtk_entry_get_text (GTK_ENTRY (ui_add_dialog->priv->title_entry)));

		/* No options to get as of yet, so just set it as empty */
		new_reminder->reminder_misc = NULL;

		break;

	default:
		g_warning ("Wrong reminder_type specified");
	}

}

static void
reminder_ui_add_dialog_clear_prio_det (ReminderUIAddDialog *ui_add_dialog)
{

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ui_add_dialog->priv->notify_notification_radio),
				      TRUE);

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ui_add_dialog->priv->sound_default_radio),
				      TRUE);

	gtk_entry_set_text (GTK_ENTRY (ui_add_dialog->priv->script_text_box),
			    "");
}

static void
reminder_ui_add_dialog_clear_fields (ReminderUIAddDialog *ui_add_dialog)
{
	gtk_entry_set_text (GTK_ENTRY (ui_add_dialog->priv->title_entry), "");

	gtk_spin_button_set_value (GTK_SPIN_BUTTON
				   (ui_add_dialog->priv->date_day_spin), 1);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON
				   (ui_add_dialog->priv->date_month_spin), 1);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON
				   (ui_add_dialog->priv->date_year_spin),
				   2009);

	gtk_spin_button_set_value (GTK_SPIN_BUTTON
				   (ui_add_dialog->priv->time_hour_spin), 0);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON
				   (ui_add_dialog->priv->time_minute_spin),
				   0);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON
				   (ui_add_dialog->priv->time_second_spin),
				   0);

	gtk_combo_box_set_active (GTK_COMBO_BOX (ui_add_dialog->priv->set_prio_cmbbox),
				  1);

	reminder_ui_add_dialog_clear_prio_det (ui_add_dialog);
}

static gboolean
reminder_ui_add_dialog_check_data (ReminderUIAddDialog *ui_add_dialog,
				   time_t *epoch_time)
{
	gint day, month, year;
	gint hour, minute, second;
	struct tm new_time;
	/* time_t epoch_time; */

	gboolean ret = FALSE;

	hour = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (ui_add_dialog->priv->time_hour_spin));
	minute = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (ui_add_dialog->priv->time_minute_spin));
	second = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (ui_add_dialog->priv->time_second_spin));

	year = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (ui_add_dialog->priv->date_year_spin));
	month = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (ui_add_dialog->priv->date_month_spin));
	day = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON	(ui_add_dialog->priv->date_day_spin));

	new_time.tm_hour = hour;
	new_time.tm_min = minute;
	new_time.tm_sec = second;

	new_time.tm_mday = day;
	new_time.tm_mon = month - 1;
	new_time.tm_year = year - 1900;
	new_time.tm_isdst = -1;

	if ((*epoch_time = mktime (&new_time)) != -1)
		ret = TRUE;

	switch (month) {
	case 4:
	case 6:
	case 9:
	case 11:
		if (day > 30)
			ret = FALSE;
		break;

	default:
		break;
	}

	if (!(year % 4) && month == 2 && day > 29)
		ret = FALSE;
	else if (month == 2 && day > 28)
		ret = FALSE;

	return ret;
}

/* g_int_equal does not work properly, so this small replacement,
 * specialist function is implemented
 */
static int
reminder_ui_add_dialog_cmp_epoch (gconstpointer a,
				  gconstpointer b)
{
	glong *epoch_first = (glong *) a;
	glong *epoch_second = (glong *) b;

	if (*epoch_first == *epoch_second)
		return 0;
	else
		return -1;

}

static void
reminder_ui_add_dialog_pressed_cb (GtkDialog *dialog,
				   gint response_id,
				   ReminderUIAddDialog *ui_add_dialog)
{
	time_t epoch_time;

	switch (response_id) {

	case GTK_RESPONSE_ACCEPT:
		ui_add_dialog->priv->ok_button_pressed = TRUE;

		if (!strlen (gtk_entry_get_text (GTK_ENTRY (ui_add_dialog->priv->title_entry))))
			{
				reminder_ui_notice_dialog_show_error (GTK_WINDOW (dialog),
								      NEW_REMINDER_TITLE_ERR);
				return;
			}
		if (!reminder_ui_add_dialog_check_data
		    (ui_add_dialog, &epoch_time))
			{
				reminder_ui_notice_dialog_show_error (GTK_WINDOW (dialog),
								      NEW_REMINDER_VALIDATION_ERR);
				return;
			}
		if (epoch_time < time (NULL))
			{
				reminder_ui_notice_dialog_show_error (GTK_WINDOW (dialog),
								  NEW_REMINDER_TIME_ERR);
				return;
			}
		if (g_list_find_custom (ui_add_dialog->priv->current_reminder_list,
					&epoch_time,
					reminder_ui_add_dialog_cmp_epoch))
			{
				reminder_ui_notice_dialog_show_error (GTK_WINDOW (dialog),
								      NEW_REMINDER_EPOCH_ERR);

				return;
			}
		break;

	default:
		ui_add_dialog->priv->ok_button_pressed = FALSE;
		break;
	}

	gtk_widget_hide (GTK_WIDGET (dialog));
	gtk_main_quit ();
}

static void
reminder_ui_add_dialog_calendar_pressed_cb (GtkWidget *emitting_widget,
					    ReminderUIAddDialog *ui_add_dialog)
{
	guint day, month, year;

	gtk_calendar_get_date (GTK_CALENDAR (emitting_widget),
			       &year, &month, &day);

	gtk_spin_button_set_value (GTK_SPIN_BUTTON (ui_add_dialog->priv->date_day_spin),
				   day);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (ui_add_dialog->priv->date_month_spin),
				   ++month);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (ui_add_dialog->priv->date_year_spin),
				   year);
}

static void
reminder_ui_add_dialog_show_file_chooser_cb (GtkWidget *button,
					     ReminderUIAddDialog *ui_add_dialog)
{
	gchar *file_name;

	gtk_widget_show_all (ui_add_dialog->priv->file_chooser_dialog);

	switch (gtk_dialog_run (GTK_DIALOG (ui_add_dialog->priv->file_chooser_dialog))) {

	case GTK_RESPONSE_ACCEPT:
		if ((file_name = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (ui_add_dialog->priv->file_chooser_dialog))))
			{
				gtk_entry_set_text (GTK_ENTRY (ui_add_dialog->priv->script_text_box),
						    file_name);
				g_free (file_name);
			}

	default:
		gtk_widget_hide (ui_add_dialog->priv->file_chooser_dialog);
		return;
	}

}

static void
reminder_ui_add_dialog_cmbbox_changed_cb (GtkComboBox *cmb_box,
					  ReminderUIAddDialog *ui_add_dialog)
{
	if (gtk_combo_box_get_active (GTK_COMBO_BOX (cmb_box)) == CUSTOM_PRIORITY)
		gtk_widget_set_sensitive (ui_add_dialog->priv->set_prio_det_button,
					  TRUE);
	else
		gtk_widget_set_sensitive (ui_add_dialog->priv->set_prio_det_button,
					  FALSE);
}

static void
reminder_ui_add_dialog_show_prio_det_dialog (GtkWidget *button, ReminderUIAddDialog *ui_add_dialog)
{
	gtk_widget_show_all (ui_add_dialog->priv->prio_det_dialog);
	gtk_dialog_run (GTK_DIALOG (ui_add_dialog->priv->prio_det_dialog));
	gtk_widget_hide (ui_add_dialog->priv->prio_det_dialog);
}

void
reminder_ui_add_dialog_prep_finalize (ReminderUIAddDialog *ui_add_dialog)
{
	if (gtk_widget_get_visible (ui_add_dialog->priv->prio_det_dialog)) {
		gtk_dialog_response (GTK_DIALOG (ui_add_dialog->priv->prio_det_dialog),
				     GTK_RESPONSE_DELETE_EVENT);
	}

	if (gtk_widget_get_visible (ui_add_dialog->priv->dialog)) {
		gtk_dialog_response (GTK_DIALOG (ui_add_dialog->priv->dialog),
				     GTK_RESPONSE_DELETE_EVENT);
	}
}

static void
reminder_ui_add_dialog_finalize (GObject *object)
{
	ReminderUIAddDialog *ui_add_dialog = REMINDER_UI_ADD_DIALOG (object);

	gtk_widget_destroy (ui_add_dialog->priv->dialog);

	g_object_unref (ui_add_dialog->priv->set_prio_list);
}

static void
reminder_ui_add_dialog_class_init (ReminderUIAddDialogClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = reminder_ui_add_dialog_finalize;

	g_type_class_add_private (klass, sizeof (ReminderUIAddDialogPrivate));
}

static void
reminder_ui_add_dialog_build_prio_det_dialog (ReminderUIAddDialog *ui_add_dialog)
{
	GtkWidget *section_alignment,
		*sub_alignment,
		*sub_box,
		*section_label;
	gchar *markup_text;
	GtkFileFilter *script_filter;

	     ui_add_dialog->priv->prio_det_dialog =
		     gtk_dialog_new_with_buttons (_("Set Priority Details"),
						  GTK_WINDOW (ui_add_dialog->priv->dialog),
						  GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						  GTK_STOCK_OK,
						  GTK_RESPONSE_ACCEPT,
						  NULL);
	     /* gtk_dialog_set_has_separator (GTK_DIALOG (ui_add_dialog->priv->prio_det_dialog), */
	     /* 				   FALSE); */
	     gtk_window_set_resizable (GTK_WINDOW (ui_add_dialog->priv->prio_det_dialog),
				       FALSE);
	     gtk_window_set_icon_name (GTK_WINDOW (ui_add_dialog->priv->prio_det_dialog),
				       REMINDER_NG_APP_ICON);

	     ui_add_dialog->priv->file_chooser_dialog =
		     gtk_file_chooser_dialog_new (_("Select a Script"),
						  GTK_WINDOW (ui_add_dialog->priv->prio_det_dialog),
						  GTK_FILE_CHOOSER_ACTION_OPEN,
						  GTK_STOCK_CANCEL,
						  GTK_RESPONSE_REJECT,
						  GTK_STOCK_OK,
						  GTK_RESPONSE_ACCEPT,
						  NULL);
	     gtk_window_set_destroy_with_parent (GTK_WINDOW (ui_add_dialog->priv->file_chooser_dialog),
						 TRUE);

	     /* Only show shellscript files in the file chooser dialog */
	     script_filter = gtk_file_filter_new ();
	     gtk_file_filter_add_mime_type (script_filter,
					    "application/x-shellscript");

	     gtk_file_chooser_set_filter (GTK_FILE_CHOOSER (ui_add_dialog->priv->file_chooser_dialog),
					  script_filter);

	     /* Visual Alarm section */

	     /* Section title */
	     markup_text = g_markup_printf_escaped (_("<b>Visual Alarm</b>"));

	     ui_add_dialog->priv->notify_type_title = gtk_label_new ("");
	     gtk_label_set_markup (GTK_LABEL (ui_add_dialog->priv->notify_type_title),
				   markup_text);
	     g_free (markup_text);

	     section_alignment = gtk_alignment_new (0, 0,
						    0, 0);
	     gtk_alignment_set_padding (GTK_ALIGNMENT (section_alignment),
					6, 0,
					12, 12);
	     gtk_container_add (GTK_CONTAINER (section_alignment),
				ui_add_dialog->priv->notify_type_title);

	     gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (ui_add_dialog->priv->prio_det_dialog))),
				 section_alignment,
				 FALSE,
				 FALSE,
				 0);
	     /* End of section title */

	     /* Subsection */
	     sub_box = gtk_vbox_new (FALSE,
				      0);

	     ui_add_dialog->priv->notify_off_radio = gtk_radio_button_new_with_mnemonic (NULL,
											 _("_None"));
	     ui_add_dialog->priv->notify_notification_radio =
		     gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON (ui_add_dialog->priv->notify_off_radio),
								     _("No_tification"));
	     ui_add_dialog->priv->notify_popup_radio =
		     gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON (ui_add_dialog->priv->notify_off_radio),
								     _("_Popup Window"));


	     gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ui_add_dialog->priv->notify_notification_radio),
					   TRUE);

	     gtk_box_pack_start (GTK_BOX (sub_box),
				 ui_add_dialog->priv->notify_off_radio,
				 FALSE,
				 FALSE,
				 0);
	     gtk_box_pack_start (GTK_BOX (sub_box),
				 ui_add_dialog->priv->notify_notification_radio,
				 FALSE,
				 FALSE,
				 0);
	     gtk_box_pack_start (GTK_BOX (sub_box),
				 ui_add_dialog->priv->notify_popup_radio,
				 FALSE,
				 FALSE,
				 0);

	     sub_alignment = gtk_alignment_new (0, 0,
						0, 0);
	     gtk_alignment_set_padding (GTK_ALIGNMENT (sub_alignment),
					0, 6,
					24, 24);
	     gtk_container_add (GTK_CONTAINER (sub_alignment),
				sub_box);

	     gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (ui_add_dialog->priv->prio_det_dialog))),
				 sub_alignment,
				 FALSE,
				 FALSE,
				 0);
	     /* End of subsection */

	     /* End of Visual Alarm section */


	     /* Sound Alarm section */

	     /* Section title */
	     markup_text = g_markup_printf_escaped (_("<b>Sound Alarm</b>"));

	     ui_add_dialog->priv->sound_play_title = gtk_label_new ("");
	     gtk_label_set_markup (GTK_LABEL (ui_add_dialog->priv->sound_play_title),
				   markup_text);
	     g_free (markup_text);

	     section_alignment = gtk_alignment_new (0, 0,
						    0, 0);
	     gtk_alignment_set_padding (GTK_ALIGNMENT (section_alignment),
					6, 0,
					12, 12);
	     gtk_container_add (GTK_CONTAINER (section_alignment),
				ui_add_dialog->priv->sound_play_title);

	     gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (ui_add_dialog->priv->prio_det_dialog))),
				 section_alignment,
				 FALSE,
				 FALSE,
				 0);
	     /* End of section title */

	     /* Subsection */
	     sub_box = gtk_vbox_new (FALSE,
				      0);

	     ui_add_dialog->priv->sound_off_radio = gtk_radio_button_new_with_mnemonic (NULL,
											_("N_one"));

	     ui_add_dialog->priv->sound_default_radio =
		     gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON (ui_add_dialog->priv->sound_off_radio),
								     _("_Default"));
	     gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ui_add_dialog->priv->sound_default_radio),
					   TRUE);

	     gtk_box_pack_start (GTK_BOX (sub_box),
				 ui_add_dialog->priv->sound_off_radio,
				 FALSE,
				 FALSE,
				 0);
	     gtk_box_pack_start (GTK_BOX (sub_box),
				 ui_add_dialog->priv->sound_default_radio,
				 FALSE,
				 FALSE,
				 0);

	     sub_alignment = gtk_alignment_new (0, 0,
						0, 0);
	     gtk_alignment_set_padding (GTK_ALIGNMENT (sub_alignment),
					0, 6,
					24, 24);
	     gtk_container_add (GTK_CONTAINER (sub_alignment),
				sub_box);

	     gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (ui_add_dialog->priv->prio_det_dialog))),
				 sub_alignment,
				 FALSE,
				 FALSE,
				 0);
	     /* End of subsection */

	     /* End of Sound Alarm section */


	     /* Script alarm section */

	     /* Section title */
	     markup_text = g_markup_printf_escaped (_("<b>Script Alarm</b>"));

	     section_label = gtk_label_new ("");
	     gtk_label_set_markup (GTK_LABEL (section_label),
	     			   markup_text);
	     g_free (markup_text);

	     section_alignment = gtk_alignment_new (0, 0,
						    0, 0);
	     gtk_alignment_set_padding (GTK_ALIGNMENT (section_alignment),
	     				6, 6,
	     				12, 12);
	     gtk_container_add (GTK_CONTAINER (section_alignment),
	     			section_label);

				 gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (ui_add_dialog->priv->prio_det_dialog))),
	     			 section_alignment,
	     			 FALSE,
	     			 FALSE,
	     			 0);
	     /* End of section title */

	     /* Subsection */
	     ui_add_dialog->priv->script_path_title = gtk_label_new ("Script Path:-");


	     ui_add_dialog->priv->script_text_box = gtk_entry_new ();
	     gtk_widget_set_size_request (ui_add_dialog->priv->script_text_box,
					  300,
					  -1);
	     ui_add_dialog->priv->script_set_button = gtk_button_new_with_mnemonic (_("_Browse"));

	     sub_box = gtk_hbox_new (FALSE,
				      0);

	     gtk_box_pack_start (GTK_BOX (sub_box),
	     			 ui_add_dialog->priv->script_path_title,
	     			 FALSE,
	     			 FALSE,
	     			 0);
	     gtk_box_pack_start (GTK_BOX (sub_box),
	     			 ui_add_dialog->priv->script_text_box,
	     			 FALSE,
	     			 FALSE,
	     			 6);
	     gtk_box_pack_start (GTK_BOX (sub_box),
	     			 ui_add_dialog->priv->script_set_button,
	     			 FALSE,
	     			 FALSE,
	     			 0);

	     sub_alignment = gtk_alignment_new (0, 0,
						0, 0);
	     gtk_alignment_set_padding (GTK_ALIGNMENT (sub_alignment),
	     				0, 12,
	     				30, 24);
	     gtk_container_add (GTK_CONTAINER (sub_alignment),
	     			sub_box);

	     gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (ui_add_dialog->priv->prio_det_dialog))),
	     			 sub_alignment,
	     			 FALSE,
	     			 FALSE,
	     			 0);
	     /* End of subsection */

	     /* End of Script Alarm section */

	     g_signal_connect (ui_add_dialog->priv->script_set_button,
			       "clicked",
			       G_CALLBACK (reminder_ui_add_dialog_show_file_chooser_cb),
			       ui_add_dialog);
}

static void
reminder_ui_add_dialog_init (ReminderUIAddDialog *ui_add_dialog)
{
	GtkWidget *section_alignment,
		*sub_alignment,
		*section_label;
	gchar *markup_text;
	GtkTreeIter list_iter;
	GtkCellRenderer *icon_renderer,
		*text_renderer;

	ui_add_dialog->priv =
		REMINDER_UI_ADD_DIALOG_GET_PRIVATE (ui_add_dialog);

	ui_add_dialog->priv->ok_button_pressed = FALSE;

	ui_add_dialog->priv->dialog =
		gtk_dialog_new_with_buttons (_("New Reminder"),
					     NULL,
					     GTK_DIALOG_MODAL,
					     GTK_STOCK_CANCEL,
					     GTK_RESPONSE_REJECT,
					     GTK_STOCK_OK,
					     GTK_RESPONSE_ACCEPT,
					     NULL);
	gtk_window_set_icon_name (GTK_WINDOW (ui_add_dialog->priv->dialog),
				  REMINDER_NG_APP_ICON);

	gtk_window_set_resizable (GTK_WINDOW (ui_add_dialog->priv->dialog),
				  FALSE);
	/* gtk_dialog_set_has_separator (GTK_DIALOG (ui_add_dialog->priv->dialog), */
	/* 			      FALSE); */

	/* Reminder details section */

	/* Section details */

	markup_text = g_markup_printf_escaped (_("<b>Reminder Details</b>"));
	section_label = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (section_label),
			      markup_text);
	g_free (markup_text);

	section_alignment = gtk_alignment_new (0, 0,
					       0, 0);
	gtk_alignment_set_padding (GTK_ALIGNMENT (section_alignment),
				   6, 6,
				   6, 6);
	gtk_container_add (GTK_CONTAINER (section_alignment),
			   section_label);

	gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (ui_add_dialog->priv->dialog))),
			    section_alignment, FALSE, FALSE, 0);
	/* End of section details */

	/* Subsection details */
	/* Reminder Title */
	sub_alignment = gtk_alignment_new (0, 0,
					   0, 0);
	gtk_alignment_set_padding (GTK_ALIGNMENT (sub_alignment),
				   0, 0,
				   12, 12);

	ui_add_dialog->priv->hbox_title = gtk_hbox_new (FALSE, 0);
	ui_add_dialog->priv->title_label = gtk_label_new (_("Title:-"));
	ui_add_dialog->priv->title_entry = gtk_entry_new ();

	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_title),
			    ui_add_dialog->priv->title_label,
			    FALSE, FALSE, 6);
	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_title),
			    ui_add_dialog->priv->title_entry,
			    FALSE, FALSE, 0);

	gtk_container_add (GTK_CONTAINER (sub_alignment),
			   ui_add_dialog->priv->hbox_title);
	gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (ui_add_dialog->priv->dialog))),
			    sub_alignment, FALSE, FALSE, 6);
	/* End of Reminder Title */
	/* End of subsection details */

	/* Subsubsection details */
	/* A new word was invented by way of recursive thinking. Werd :P */

	markup_text = g_markup_printf_escaped (_("<b>Alarm Details</b>"));
	section_label = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (section_label),
			      markup_text);
	g_free (markup_text);

	sub_alignment = gtk_alignment_new (0, 0,
					   0, 0);
	gtk_alignment_set_padding (GTK_ALIGNMENT (sub_alignment),
				   6, 6,
				   18, 18);

	gtk_container_add (GTK_CONTAINER (sub_alignment),
			   section_label);

	gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (ui_add_dialog->priv->dialog))),
			    sub_alignment, FALSE, FALSE, 0);

	/* Time details */
	ui_add_dialog->priv->hbox_time = gtk_hbox_new (FALSE, 0);
	ui_add_dialog->priv->time_label = gtk_label_new (_("Time:-"));
	ui_add_dialog->priv->time_hour_spin =
		gtk_spin_button_new_with_range (0, 23, 1);
	gtk_widget_set_size_request (ui_add_dialog->priv->time_hour_spin, 45,
				     -1);
	ui_add_dialog->priv->colon_1_label = gtk_label_new (":");
	ui_add_dialog->priv->time_minute_spin =
		gtk_spin_button_new_with_range (0, 59, 1);
	gtk_widget_set_size_request (ui_add_dialog->priv->time_minute_spin,
				     45, -1);
	ui_add_dialog->priv->colon_2_label = gtk_label_new (":");
	ui_add_dialog->priv->time_second_spin =
		gtk_spin_button_new_with_range (0, 59, 1);
	gtk_widget_set_size_request (ui_add_dialog->priv->time_second_spin,
				     45, -1);

	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_time),
			    ui_add_dialog->priv->time_label, FALSE, FALSE, 6);
	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_time),
			    ui_add_dialog->priv->time_hour_spin,
			    FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_time),
			    ui_add_dialog->priv->colon_1_label,
			    FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_time),
			    ui_add_dialog->priv->time_minute_spin,
			    FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_time),
			    ui_add_dialog->priv->colon_2_label,
			    FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_time),
			    ui_add_dialog->priv->time_second_spin,
			    FALSE, FALSE, 0);

	sub_alignment = gtk_alignment_new (0, 0,
					   0, 0);
	gtk_alignment_set_padding (GTK_ALIGNMENT (sub_alignment),
				   0, 12,
				   24, 24);

	gtk_container_add (GTK_CONTAINER (sub_alignment),
			   ui_add_dialog->priv->hbox_time);
	gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (ui_add_dialog->priv->dialog))),
			    sub_alignment, FALSE, FALSE, 0);

	/* Date details */
	ui_add_dialog->priv->hbox_date = gtk_hbox_new (FALSE, 0);
	ui_add_dialog->priv->date_label = gtk_label_new (_("Date:-"));
	ui_add_dialog->priv->date_day_spin =
		gtk_spin_button_new_with_range (1, 31, 1);
	gtk_widget_set_size_request (ui_add_dialog->priv->date_day_spin,
				     45,
				     -1);
	ui_add_dialog->priv->backslash_1_label = gtk_label_new ("/");
	ui_add_dialog->priv->date_month_spin =
		gtk_spin_button_new_with_range (1, 12, 1);
	gtk_widget_set_size_request (ui_add_dialog->priv->date_month_spin,
				     45,
				     -1);
	ui_add_dialog->priv->backslash_2_label = gtk_label_new ("/");
	ui_add_dialog->priv->date_year_spin =
		gtk_spin_button_new_with_range (2009, 2036, 1);
	gtk_widget_set_size_request (ui_add_dialog->priv->date_year_spin,
				     70,
				     -1);

	ui_add_dialog->priv->date_select_expander =
		gtk_expander_new_with_mnemonic (_("Select _Date:"));
	ui_add_dialog->priv->date_calendar = gtk_calendar_new ();

	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_date),
			    ui_add_dialog->priv->date_label, FALSE, FALSE, 6);
	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_date),
			    ui_add_dialog->priv->date_day_spin,
			    FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_date),
			    ui_add_dialog->priv->backslash_1_label,
			    FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_date),
			    ui_add_dialog->priv->date_month_spin,
			    FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_date),
			    ui_add_dialog->priv->backslash_2_label,
			    FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_date),
			    ui_add_dialog->priv->date_year_spin,
			    FALSE, FALSE, 0);

	gtk_container_add (GTK_CONTAINER (ui_add_dialog->priv->date_select_expander),
			   ui_add_dialog->priv->date_calendar);

	sub_alignment = gtk_alignment_new (0, 0,
					   0, 0);
	gtk_alignment_set_padding (GTK_ALIGNMENT (sub_alignment),
				   0, 6,
				   24, 24);

	gtk_container_add (GTK_CONTAINER (sub_alignment),
			   ui_add_dialog->priv->hbox_date);
	gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (ui_add_dialog->priv->dialog))),
			    sub_alignment, FALSE, FALSE, 0);

	sub_alignment = gtk_alignment_new (0, 0,
					   0, 0);
	gtk_alignment_set_padding (GTK_ALIGNMENT (sub_alignment),
				   0, 12,
				   24, 24);

	gtk_container_add (GTK_CONTAINER (sub_alignment),
			   ui_add_dialog->priv->date_select_expander);
	gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (ui_add_dialog->priv->dialog))),
			    sub_alignment, FALSE, FALSE, 0);

	/* Priority details */
	ui_add_dialog->priv->hbox_prio = gtk_hbox_new (FALSE, 0);

	ui_add_dialog->priv->set_prio_label = gtk_label_new (_("Priority:-"));

	ui_add_dialog->priv->set_prio_list = gtk_list_store_new (2,
								 G_TYPE_STRING,
								 G_TYPE_STRING);

	gtk_list_store_append (ui_add_dialog->priv->set_prio_list,
			       &list_iter);
	gtk_list_store_set (ui_add_dialog->priv->set_prio_list, &list_iter,
			    0, REMINDER_NG_PRIO_HIGH_ICON,
			    1, _("High"),
			    -1);
	gtk_list_store_append (ui_add_dialog->priv->set_prio_list,
			       &list_iter);
	gtk_list_store_set (ui_add_dialog->priv->set_prio_list, &list_iter,
			    0, REMINDER_NG_PRIO_NORMAL_ICON,
			    1, _("Normal"),
			    -1);
	gtk_list_store_append (ui_add_dialog->priv->set_prio_list,
			       &list_iter);
	gtk_list_store_set (ui_add_dialog->priv->set_prio_list, &list_iter,
			    0, REMINDER_NG_PRIO_LOW_ICON,
			    1, _("Low"),
			    -1);
	gtk_list_store_append (ui_add_dialog->priv->set_prio_list,
			       &list_iter);
	gtk_list_store_set (ui_add_dialog->priv->set_prio_list, &list_iter,
			    0, REMINDER_NG_PRIO_CUSTOM_ICON,
			    1, _("Custom"),
			    -1);

	ui_add_dialog->priv->set_prio_cmbbox = gtk_combo_box_new_with_model (GTK_TREE_MODEL (ui_add_dialog->priv->set_prio_list));

	icon_renderer = (GtkCellRenderer *) gtk_cell_renderer_pixbuf_new ();
	text_renderer = (GtkCellRenderer *) gtk_cell_renderer_text_new ();

	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (ui_add_dialog->priv->set_prio_cmbbox),
				    icon_renderer,
				    FALSE);
	gtk_cell_layout_add_attribute (GTK_CELL_LAYOUT (ui_add_dialog->priv->set_prio_cmbbox),
				       icon_renderer,
				       "icon-name", 0);
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (ui_add_dialog->priv->set_prio_cmbbox),
				    text_renderer,
				    TRUE);
	gtk_cell_layout_add_attribute (GTK_CELL_LAYOUT (ui_add_dialog->priv->set_prio_cmbbox),
				       text_renderer,
				       "text", 1);
	/* ui_add_dialog->priv->set_prio_cmbbox = gtk_combo_box_new_text (); */
	/* gtk_combo_box_append_text (GTK_COMBO_BOX (ui_add_dialog->priv->set_prio_cmbbox), */
	/* 			   _("High")); */
	/* gtk_combo_box_append_text (GTK_COMBO_BOX (ui_add_dialog->priv->set_prio_cmbbox), */
	/* 			   _("Normal")); */
	/* gtk_combo_box_append_text (GTK_COMBO_BOX (ui_add_dialog->priv->set_prio_cmbbox), */
	/* 			   _("Low")); */
	/* gtk_combo_box_append_text (GTK_COMBO_BOX (ui_add_dialog->priv->set_prio_cmbbox), */
	/* 			   _("Custom")); */
	gtk_combo_box_set_active (GTK_COMBO_BOX (ui_add_dialog->priv->set_prio_cmbbox),
				  1);

	ui_add_dialog->priv->set_prio_det_button =
		gtk_button_new_with_mnemonic (_("Set _Details"));

	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_prio),
			    ui_add_dialog->priv->set_prio_label,
			    FALSE, FALSE, 6);
	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_prio),
			    ui_add_dialog->priv->set_prio_cmbbox,
			    FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (ui_add_dialog->priv->hbox_prio),
			    ui_add_dialog->priv->set_prio_det_button,
			    FALSE, FALSE, 12);

	sub_alignment = gtk_alignment_new (0, 0,
					   0, 0);
	gtk_alignment_set_padding (GTK_ALIGNMENT (sub_alignment),
				   0, 12,
				   24, 24);

	gtk_container_add (GTK_CONTAINER (sub_alignment),
			   ui_add_dialog->priv->hbox_prio);
	gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (ui_add_dialog->priv->dialog))),
			    sub_alignment, FALSE, FALSE, 0);
	/* End of subsubsection details */

	reminder_ui_add_dialog_build_prio_det_dialog (ui_add_dialog);

	g_signal_connect (ui_add_dialog->priv->date_calendar,
			  "day-selected",
			  G_CALLBACK
			  (reminder_ui_add_dialog_calendar_pressed_cb),
			  ui_add_dialog);
	g_signal_connect (ui_add_dialog->priv->dialog,
			  "response",
			  G_CALLBACK (reminder_ui_add_dialog_pressed_cb),
			  ui_add_dialog);
	g_signal_connect (ui_add_dialog->priv->set_prio_cmbbox,
			  "changed",
			  G_CALLBACK (reminder_ui_add_dialog_cmbbox_changed_cb),
			  ui_add_dialog);
	g_signal_connect (ui_add_dialog->priv->set_prio_det_button,
			  "clicked",
			  G_CALLBACK (reminder_ui_add_dialog_show_prio_det_dialog),
			  ui_add_dialog);
}

ReminderUIAddDialog *
reminder_ui_add_dialog_new ()
{
	ReminderUIAddDialog *ui_add_dialog;

	ui_add_dialog = g_object_new (REMINDER_TYPE_UI_ADD_DIALOG, NULL);

	return ui_add_dialog;
}
