/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <glib/gi18n.h>
#include <glib-2.0/glib.h>
#include <glib-object.h>

#include "reminder-list.h"
#include "reminder-file-ops.h"

/* Format of database file(still subject to change):-
 * "%d\n"       - Reminder priority
 * "%s\n"       - Reminder title
 * "%d:%d:%d\n" - Hour | Minute | Second
 * "%d/%d/%d\n" - Day  | Month  | Year
 * "%d\n"       - Timezone
 * "%ld\n"      - Time since epoch(made using the broken down represented time)
 * "%s\n\n"     - Miscellanous stuff concerning the reminder
 *
 * If the reminder priority is equal to 6, then this is the format (The extras
 * concerning what should be done when the reminder expires):-
 * "%d\n"       - Reminder priority
 * "%s\n"       - Reminder title
 * "%d:%d:%d\n" - Hour | Minute | Second
 * "%d/%d/%d\n" - Day  | Month  | Year
 * "%d\n"       - Timezone
 * "%ld\n"      - Time since epoch(made using the broken down represented time)
 * "%s\n"       - Miscellanous stuff concerning the reminder
 * "%d\n"       - What notification type to show
 * "%d\n"       - Whether a sound should be played
 * "%s\n\n"     - Whether a script should be played
 */

int
flush_reminders_file (FILE *file, ReminderList *reminder_list)
{
	gchar *reminder_misc, *reminder_misc_old,
		*script_path;
	guint i, x;

	while (reminder_list != NULL)
	  {

		  if (reminder_list->reminder_misc) {

			  for (i = 0, x = g_list_length (reminder_list->reminder_misc);
			       i < x;
			       i++)
				  {

					  if (!i)
						  reminder_misc = g_strdup_printf ("%s",
										   (gchar *) g_list_nth_data (reminder_list->reminder_misc,
													i));
					  else {
						  reminder_misc_old = reminder_misc;

						  reminder_misc = g_strdup_printf ("%s|%s",
										   reminder_misc_old,
										   (gchar *) g_list_nth_data (reminder_list->reminder_misc,
													i));
						  g_free (reminder_misc_old);
					  }
				  }
		  }
		  else
			  reminder_misc = g_strdup ("-");

		  if (reminder_list->reminder_prio == 6) {
			  if (reminder_list->prio_det->script_path)
				  script_path = g_strdup (reminder_list->prio_det->script_path);
			  else
				  script_path = g_strdup ("-");
		  }

		  if (reminder_list->reminder_prio == 6) {
			  fprintf (file,
				   "%d\n"
				   "%s\n"
				   "%d:%d:%d\n"
				   "%d/%d/%d\n"
				   "%d\n"
				   "%d\n"
				   "%s\n"
				   "%d\n"
				   "%d\n"
				   "%s\n\n",
				   reminder_list->reminder_prio,
				   reminder_list->reminder_title,
				   reminder_list->reminder_hour,
				   reminder_list->reminder_minute,
				   reminder_list->reminder_second,
				   reminder_list->reminder_day,
				   reminder_list->reminder_month,
				   reminder_list->reminder_year,
				   reminder_list->reminder_tz,
				   reminder_list->reminder_epoch,
				   reminder_misc,
				   reminder_list->prio_det->vis_alrm_type,
				   reminder_list->prio_det->snd_alrm_play,
				   script_path);

			  g_free (script_path);
		  }
		  else
			  fprintf (file,
				   "%d\n"
				   "%s\n"
				   "%d:%d:%d\n"
				   "%d/%d/%d\n"
				   "%d\n"
				   "%d\n"
				   "%s\n\n",
				   reminder_list->reminder_prio,
				   reminder_list->reminder_title,
				   reminder_list->reminder_hour,
				   reminder_list->reminder_minute,
				   reminder_list->reminder_second,
				   reminder_list->reminder_day,
				   reminder_list->reminder_month,
				   reminder_list->reminder_year,
				   reminder_list->reminder_tz,
				   reminder_list->reminder_epoch,
				   reminder_misc);

		  reminder_list = reminder_list->next;
		  g_free (reminder_misc);
	  }

	fflush (file);

	return 0;
}

FILE *
open_reminder_database (void)
{
	gchar *database_dir_path, *database_path;
	FILE *database;
	struct stat temp_dir_buf;

	database_dir_path = g_strconcat (getenv ("HOME"),
					 "/.reminder-ng",
					 NULL);

	if (stat (database_dir_path, &temp_dir_buf))
		mkdir (database_dir_path, S_IRWXU);

	database_path =	g_strconcat (database_dir_path,
				     "/reminder.conf",
				     NULL);

	database = fopen (database_path, "a+");

	if (database == NULL) {
		printf (_("Error opening reminder database: %s\n"
			  "Path: %s\n"),
			strerror (errno),
			database_path);

		exit (EXIT_FAILURE);
	}

	g_free (database_path);
	g_free (database_dir_path);

	return database;
}

static int
get_reminder_entry_from_database (FILE *database,
				  ReminderList **reminder_list,
				  gboolean discarded_reminders)
{
	gint fscanf_ret, fscanf_ret_temp;

	/* Buffers for the reminder entry details */
	gint reminder_prio,
		reminder_hour,
		reminder_minute,
		reminder_second,
		reminder_day,
		reminder_month,
		reminder_year,
		reminder_tz,
		reminder_epoch,
		notify_type;

	gboolean sound_play;

	ReminderList *added_node;

	fpos_t database_pos;

	/* A maximum of 1024 bytes allocated for the title initially
	 * and then clipped before putting it in the list and 20
	 * for any miscellanous reminder settings which are treated
	 * same as the reminder title (size includes the nul character)
	 */
	gchar reminder_title[1024],
		reminder_misc[20],
		script_path[1024];

	/* Get the current offset from the beginning of the file
	 * so that we can return to it if the discarded reminders end
	 * (Only useful when discarded_reminders == TRUE)
	 */
	fgetpos (database, &database_pos);

	if ((fscanf_ret = fscanf (database, "%d\n", &reminder_prio)) == EOF)
	  {
		  goto out;
	  }

	if (fgets (reminder_title, 1024, database))
		*(strchr (reminder_title, '\n')) = '\0';
	else
	  {
		  fscanf_ret = EOF;
		  goto out;
	  }

	fscanf_ret = fscanf (database,
			     "%d:%d:%d\n"
			     "%d/%d/%d\n"
			     "%d\n"
			     "%d\n",
			     &reminder_hour,
			     &reminder_minute,
			     &reminder_second,
			     &reminder_day,
			     &reminder_month,
			     &reminder_year,
			     &reminder_tz,
			     &reminder_epoch);

	if (fgets (reminder_misc,
		   20,
		   database))
		*(strchr (reminder_misc, '\n')) = '\0';
	else {
		/* If nothing was read, not even the compulsory "-" then
		 * stop loading the reminders
		 */
		fscanf_ret = EOF;
		goto out;
	}

	if (reminder_prio == 6) {

		if ((fscanf_ret_temp = fscanf (database,
					       "%d\n"
					       "%d\n",
					       &notify_type,
					       &sound_play)) == EOF)
			{
				fscanf_ret = EOF;

				goto out;
			}
		/* Make sure that the required number of things were read */
		else if (fscanf_ret_temp == 2)
			fscanf_ret += fscanf_ret_temp;

		else {
			fscanf_ret = EOF;

			goto out;
		}

		if (fgets (script_path,
			   1024,
			   database))
			*(strchr (script_path, '\n')) = '\0';
		else {
			/* If nothing was read, not even the compulsory "-" then
			 * stop loading the reminders
			 */
			fscanf_ret = EOF;

			goto out;
		}
	}

	/* Move on to the next reminder */
	if (fscanf (database,
		    "\n") == EOF) {
		fscanf_ret = EOF;
		goto out;
	}

	switch (fscanf_ret)
	  {

		  /* When the reminder priority is not equal to 6 */
	  case 8:
		  if (discarded_reminders == TRUE
		      && reminder_epoch < time (NULL))
		    {
			    /* Discarded reminders */
			    reminder_list_add_node (reminder_list,
						    reminder_prio,
						    reminder_title,
						    reminder_hour,
						    reminder_minute,
						    reminder_second,
						    reminder_day,
						    reminder_month,
						    reminder_year,
						    reminder_tz,
						    reminder_epoch,
						    reminder_misc,
						    ASCEND_SORT);
		    }
		  else if (discarded_reminders == TRUE
			   && reminder_epoch >= time (NULL))
		    {
			    /* If it's not a discarded reminder, we go back to
			     * the original offset in the database file
			     */
			    fsetpos (database, &database_pos);
			    fscanf_ret = EOF;
			    goto out;
		    }

		  if (discarded_reminders == FALSE)
		    {
			    /* Current reminders */
			    reminder_list_add_node (reminder_list,
						    reminder_prio,
						    reminder_title,
						    reminder_hour,
						    reminder_minute,
						    reminder_second,
						    reminder_day,
						    reminder_month,
						    reminder_year,
						    reminder_tz,
						    reminder_epoch,
						    reminder_misc,
						    ASCEND_SORT);

		    }
		  break;

		  /* When the reminder priority is equal to 6 */
	  case 10:

		  if (discarded_reminders == TRUE
		      && reminder_epoch < time (NULL))
		    {
			    /* Discarded reminders */
			    added_node = reminder_list_add_node (reminder_list,
								 reminder_prio,
								 reminder_title,
								 reminder_hour,
								 reminder_minute,
								 reminder_second,
								 reminder_day,
								 reminder_month,
								 reminder_year,
								 reminder_tz,
								 reminder_epoch,
								 reminder_misc,
								 ASCEND_SORT);

			    if (g_strcmp0 (script_path, "-"))
				    reminder_list_set_prio_det (added_node,
								notify_type,
								sound_play,
								script_path);
			    else
				    reminder_list_set_prio_det (added_node,
								notify_type,
								sound_play,
								NULL);
		    }
		  else if (discarded_reminders == TRUE
			   && reminder_epoch >= time (NULL))
		    {
			    /* If it's not a discarded reminder, we go back to
			     * the original offset in the database file
			     */
			    fsetpos (database, &database_pos);
			    fscanf_ret = EOF;
			    goto out;
		    }

		  if (discarded_reminders == FALSE)
		    {
			    /* Current reminders */
			    added_node = reminder_list_add_node (reminder_list,
								 reminder_prio,
								 reminder_title,
								 reminder_hour,
								 reminder_minute,
								 reminder_second,
								 reminder_day,
								 reminder_month,
								 reminder_year,
								 reminder_tz,
								 reminder_epoch,
								 reminder_misc,
								 ASCEND_SORT);

			    if (g_strcmp0 (script_path, "-"))
				    reminder_list_set_prio_det (added_node,
								notify_type,
								sound_play,
								script_path);
			    else
				    reminder_list_set_prio_det (added_node,
								notify_type,
								sound_play,
								NULL);
		    }
		  break;

	  case EOF:
		  goto out;

	  default:
		  g_warning (_("Unable to load reminder database properly"));
		  fsetpos (database, &database_pos);
		  fscanf_ret = EOF;
	  }

      out:
	return fscanf_ret;
}

void
reminder_load_database (FILE *database,
			ReminderList **reminder_list,
			ReminderList **discarded_list)
{
	/* First obtain the discarded reminders */
	while (get_reminder_entry_from_database (database,
						 discarded_list,
						 TRUE) != EOF);

	/* Second, obtain the current reminders */
	while (get_reminder_entry_from_database (database,
						 reminder_list,
						 FALSE) != EOF);

}
