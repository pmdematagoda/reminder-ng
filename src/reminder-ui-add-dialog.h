/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib-object.h>

G_BEGIN_DECLS
#define REMINDER_TYPE_UI_ADD_DIALOG           (reminder_ui_add_dialog_get_type ())
#define REMINDER_UI_ADD_DIALOG(o)             (G_TYPE_CHECK_INSTANCE_CAST ((o), REMINDER_TYPE_UI_ADD_DIALOG, ReminderUIAddDialog))
#define REMINDER_UI_ADD_DIALOG_CLASS(k)       (G_TYPE_CHECK_CLASS_CAST ((k), REMINDER_TYPE_UI_ADD_DIALOG, ReminderUIAddDialogClass))
#define REMINDER_IS_UI_ADD_DIALOG(o)          (G_TYPE_CHECK_INSTANCE_TYPE ((o), REMINDER_TYPE_UI_ADD_DIALOG))
#define REMINDER_IS_UI_ADD_DIALOG_CLASS(k)    (G_TYPE_CHECK_CLASS_TYPE ((k), REMINDER_TYPE_UI_ADD_DIALOG))
#define REMINDER_UI_ADD_DIALOG_GET_CLASS(o)   (G_TYPE_INSTANCE_GET_CLASS ((k), REMINDER_TYPE_UI_ADD_DIALOG, ReminderUIAddDialog))
typedef struct ReminderUIAddDialogPrivate ReminderUIAddDialogPrivate;

typedef struct
{
	GObject                        parent;
	ReminderUIAddDialogPrivate    *priv;
} ReminderUIAddDialog;

typedef struct
{
	GObjectClass    parent_class;
} ReminderUIAddDialogClass;

typedef enum
{
	FIVE_MINUTE_QUICK_ADD,
	TEN_MINUTE_QUICK_ADD,
	FIFTEEN_MINUTE_QUICK_ADD,
	THIRTY_MINUTE_QUICK_ADD,
	SIXTY_MINUTE_QUICK_ADD
} ReminderUIAddDialogQuickAddType;

GType                   reminder_ui_add_dialog_get_type              (void);
ReminderUIAddDialog    *reminder_ui_add_dialog_new                   (void);
ReminderList           *reminder_ui_add_dialog_show                  (ReminderUIAddDialog *ui_add_dialog,
								      const ReminderList *reminder_list,
								      GList *current_reminder_list);
ReminderList           *reminder_ui_add_dialog_quick_add_reminder    (ReminderUIAddDialog *ui_add_dialog,
								      ReminderUIAddDialogQuickAddType quick_add_type,
								      GList *current_reminder_list);
void                    reminder_ui_add_dialog_prep_finalize         (ReminderUIAddDialog *ui_add_dialog);

G_END_DECLS
