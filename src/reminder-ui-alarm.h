/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib-object.h>

G_BEGIN_DECLS
#define TYPE_REMINDER_UI_ALARM           (reminder_ui_alarm_get_type ())
#define REMINDER_UI_ALARM(o)             (G_TYPE_CHECK_INSTANCE_CAST ((o), TYPE_REMINDER_UI_ALARM, ReminderUIAlarm))
#define REMINDER_UI_ALARM_CLASS(k)       (G_TYPE_CHECK_CLASS_CAST ((k), TYPE_REMINDER_UI_ALARM, ReminderUIAlarmClass))
#define REMINDER_IS_UI_ALARM(o)          (G_TYPE_CHECK_INSTANCE_TYPE ((o), TYPE_REMINDER_UI_ALARM))
#define REMINDER_IS_UI_ALARM_CLASS(k)    (G_TYPE_CHECK_CLASS_TYPE ((k), TYPE_REMINDER_UI_ALARM))
#define REMINDER_UI_ALARM_GET_CLASS(o)   (G_TYPE_INSTANCE_GET_CLASS ((k), TYPE_REMINDER_UI_ALARM, ReminderUIAlarm))

typedef struct ReminderUIAlarmPrivate ReminderUIAlarmPrivate;

typedef struct
{
	GObject                    parent;
	ReminderUIAlarmPrivate    *priv;
} ReminderUIAlarm;

typedef struct
{
	GObjectClass    parent_class;

        void           (*reminder_noticed)     (ReminderUIAlarm *ui_alarm);

        void           (*reminder_missed)      (ReminderUIAlarm *ui_alarm);

} ReminderUIAlarmClass;

GType              reminder_ui_alarm_get_type        (void);
ReminderUIAlarm   *reminder_ui_alarm_new             (void);
void               reminder_ui_alarm_notify          (ReminderUIAlarm *ui_alarm,
						      const gchar *reminder_title,
						      guint alarm_prio,
						      ReminderPrioDet *prio_det);
void               reminder_ui_alarm_prep_finalize   (ReminderUIAlarm *ui_alarm);

G_END_DECLS
