/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

typedef enum
{
	ASCEND_SORT,
	DESCEND_SORT
} ReminderListAddSortType;

typedef enum
{
	NORMAL_DELETE,
	FIRST_IN_LINE,
	DELETE_ALL
} ReminderListDeleteAction;

typedef enum
{
	RC_NOTICED_PARAM,
	RU_NOTIFPRES_PARAM,
	RU_PUSHEDOUT_PARAM,
	NUM_OF_PARAMS
} ReminderListMiscParams;

static const gint reminder_list_misc_params_type[] = 
{
	G_TYPE_NONE,
	G_TYPE_NONE,
	G_TYPE_NONE
};

/* Current parameters used in reminder_misc are:-
 * n  - reminder has been noted by the user
 *      (ReminderCore usage only)
 *
 * ns - the notification for the expired reminder
 *      is still being shown (ReminderUI usage only)
 *
 * po - the notification for the expired reminder
 *      has been hidden as it was _pushed out_
 *      by a more recent reminder that expired
 *      while the notification was being shown
 */

typedef struct _ReminderPrioDet
{
	/* What type of visual notification to show when the reminder expires.
	 * Possible values:
	 *
	 * 0 - No notification shown
	 *
	 * 1 - Regular notification balloon shown
	 *
	 * 2 - Popup window shown
	 */
	guint vis_alrm_type;

	/* Whether a sound should be played when the reminder expires.
	 * Possible values:
	 *
	 * FALSE - No sound is to be played
	 *
	 * TRUE -  Default sound clip is to be played
	 */
	gboolean snd_alrm_play;

	/* If this variable is non-NULL there is a script
	 * to be executed when the reminder has expired
	 */
	gchar *script_path;
} ReminderPrioDet;

typedef struct _ReminderList
{
	guint reminder_prio;
	gchar *reminder_title;

	guint reminder_hour,
		reminder_minute,
		reminder_second,
		reminder_day,
		reminder_month,
		reminder_year;

	guint reminder_tz;
	gint reminder_epoch;

	GList *reminder_misc;

	GHashTable *reminder_misc_table;

	/* This variable is set if the reminder_prio variable is
	 * equal to 6
	 */
	ReminderPrioDet *prio_det;

	struct _ReminderList *next;
} ReminderList;

ReminderList    *reminder_list_add_node               (ReminderList **reminder_list, guint reminder_prio,
						       gchar *reminder_title, guint reminder_hour,
						       guint reminder_minute, guint reminder_second,
						       guint reminder_day, guint reminder_month,
						       guint reminder_year, guint reminder_tz,
						       gint reminder_epoch, gchar *reminder_misc,
						       ReminderListAddSortType sort_type);

void             reminder_list_delete_node            (ReminderList **reminder_list,
						       ReminderListDeleteAction delete_action,
						       gint reminder_epoch);

void             reminder_list_transfer_first_node    (ReminderList **source_list,
						       ReminderList **destination_list);

void             reminder_list_free_node              (ReminderList *target_list);

void             reminder_list_misc_param_add         (ReminderList *target_node,
						       ReminderListMiscParams param_type,
						       gpointer data);

void             reminder_list_misc_param_set         (ReminderList *target_node,
						       ReminderListMiscParams param_type,
						       gpointer data);

gboolean         reminder_list_misc_param_exist       (ReminderList *target_node,
						       ReminderListMiscParams param_type);

void             reminder_list_misc_param_delete      (ReminderList *target_node,
						       ReminderListMiscParams param_type);

void             reminder_list_set_prio_det           (ReminderList *target_node,
						       guint vis_alrm_type,
						       gboolean snd_alrm_play,
						       const gchar *script_path);

GValue *       reminder_list_misc_param_get_val (ReminderList *target_node,
						 ReminderListMiscParams param_type);
