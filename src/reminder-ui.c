/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <stdlib.h>

#include <glib-2.0/glib.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <gconf/gconf-client.h>

#include "reminder-common.h"
#include "reminder-stock-icons.h"
#include "reminder-list.h"

#include "reminder-core.h"
#include "reminder-ui-add-dialog.h"
#include "reminder-ui-preferences.h"
#include "reminder-ui-notice-dialog.h"
#include "reminder-ui-alarm.h"
#include "reminder-ui.h"

#define REMINDER_UI_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), REMINDER_TYPE_UI, ReminderUIPrivate))

G_DEFINE_TYPE (ReminderUI, reminder_ui, G_TYPE_OBJECT)

struct ReminderUIPrivate
{
	GtkStatusIcon *tray_icon;

	GConfClient *gconf_client;

	ReminderList *missed_reminders;
	GtkWidget *missed_tree_view;

	GtkWidget *window;
	GtkWidget *vbox_main;
	GtkWidget *header_label;
	GtkWidget *example_label;

	GtkWidget *toolbar;

	GtkWidget *current_tree_view,
		*current_scrolled_window,
		*expired_tree_view,
		*expired_scrolled_window;

	GtkWidget *notebook;

	GtkWidget *exit_button;

	GMainLoop *main_window_loop;

	ReminderCore *reminder_core;
	ReminderUIAddDialog *reminder_ui_add_dialog;
	ReminderUIPreferences *reminder_ui_preferences;
	ReminderUIAlarm *reminder_ui_alarm;
};

typedef enum
{
	COUNTDOWN_STR,
	MISSEDBY_STR
} ReminderUIPrtyStrType;

/* List of columns in the missed tree view widget */
enum
{
	MISSED_TITLE_COL,
	MISSED_SINCE_COL,
	MISSED_EPOCH_COL,
	NO_OF_MISSED_COL
};

enum
{
	CURRENT_REMINDER_PAGE,
	EXPIRED_REMINDER_PAGE
};

enum
{
	EXIT_REQUEST_SIGNAL,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0, };

static void reminder_ui_reminder_expired_cb (ReminderCore *core,
					     glong reminder_epoch,
					     ReminderUI *ui);
static gboolean reminder_ui_tree_has_data (GtkWidget *tree_widget);
static gchar *reminder_ui_convert_epoch_to_pretty_string (gint reminder_epoch,
							  ReminderUIPrtyStrType str_type);
static gboolean reminder_ui_show_missed_reminders (ReminderUI *ui);
static gboolean reminder_ui_set_tray_icon_blink (ReminderUI *ui);
static void reminder_ui_notify_missed_reminders (ReminderUI *ui);
static void reminder_ui_build_missed_tree (ReminderUI *ui);
static void reminder_ui_prep_finalize (ReminderUI *ui);

void
reminder_ui_attach_core (ReminderUI *ui, ReminderCore *core)
{
	g_return_if_fail (ui != NULL);
	g_return_if_fail (REMINDER_IS_UI (ui));
	g_return_if_fail (core != NULL);
	g_return_if_fail (REMINDER_IS_CORE (core));

	ui->priv->reminder_core = core;

	reminder_core_build_tree (core,
				  GTK_TREE_VIEW (ui->priv->current_tree_view),
				  CURRENT_TREE);
	reminder_core_build_tree (core,
				  GTK_TREE_VIEW (ui->priv->expired_tree_view),
				  EXPIRED_TREE);

	if (reminder_ui_tree_has_data (ui->priv->current_tree_view)) {
		gtk_notebook_insert_page (GTK_NOTEBOOK (ui->priv->notebook),
					  ui->priv->current_scrolled_window,
					  gtk_label_new (_("Current Reminders")),
					  CURRENT_REMINDER_PAGE);
	}
	if (reminder_ui_tree_has_data (ui->priv->expired_tree_view)) {
		gtk_notebook_insert_page (GTK_NOTEBOOK (ui->priv->notebook),
					  ui->priv->expired_scrolled_window,
					  gtk_label_new (_("Expired Reminders")),
					  EXPIRED_REMINDER_PAGE);
	}

	reminder_core_get_missed_reminders (core,
					    &(ui->priv->missed_reminders));

	if (ui->priv->missed_reminders) {
		g_timeout_add_seconds (1,
				       (GSourceFunc) reminder_ui_set_tray_icon_blink,
				       ui);
		reminder_ui_notify_missed_reminders (ui);
		reminder_ui_build_missed_tree (ui);
	}

	g_signal_connect (core, "reminder-expired",
			  G_CALLBACK (reminder_ui_reminder_expired_cb), ui);
}

static gboolean
reminder_ui_set_tray_icon_blink (ReminderUI *ui)
{
	gboolean ret = FALSE;

	ret = !(gtk_status_icon_is_embedded (ui->priv->tray_icon));

	/* if (!ret == TRUE) */
	/* gtk_status_icon_set_blinking (ui->priv->tray_icon, */
	/* 			      TRUE); */

	return ret;
}

static void
reminder_ui_build_missed_tree (ReminderUI *ui)
{
	ReminderList *navigator_ptr;
	GtkListStore *missed_list_store;
	GtkTreeIter missed_iter;
	gchar *time_missed_str;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;

	missed_list_store = gtk_list_store_new (NO_OF_MISSED_COL,
						G_TYPE_STRING,
						G_TYPE_STRING,
						G_TYPE_INT);

	for (navigator_ptr = ui->priv->missed_reminders;
	     navigator_ptr != NULL;
	     navigator_ptr = navigator_ptr->next)
		{
			/* Skip any _missed_ reminders whose notification is still being shown.
			 * By rights, there should only be one and it should only be the first
			 */
			if (reminder_list_misc_param_exist (navigator_ptr,
							    RU_NOTIFPRES_PARAM))
				continue;

			gtk_list_store_append (missed_list_store,
					       &missed_iter);

			time_missed_str = reminder_ui_convert_epoch_to_pretty_string (navigator_ptr->reminder_epoch,
										      MISSEDBY_STR);
			gtk_list_store_set (missed_list_store,
					    &missed_iter,
					    MISSED_TITLE_COL,
					    navigator_ptr->reminder_title,
					    MISSED_SINCE_COL,
					    time_missed_str,
					    MISSED_EPOCH_COL,
					    navigator_ptr->reminder_epoch,
					    -1);

			g_free (time_missed_str);
		}
	renderer = (GtkCellRenderer *) gtk_cell_renderer_text_new ();
	ui->priv->missed_tree_view = gtk_tree_view_new ();
	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (ui->priv->missed_tree_view),
				      TRUE);

	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (ui->priv->missed_tree_view),
						     -1, _("Reminder Title"),
						     renderer, "text",
						     MISSED_TITLE_COL, NULL);

	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (ui->priv->missed_tree_view),
						     -1, _("Expired"),
						     renderer, "text",
						     MISSED_SINCE_COL, NULL);

	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (ui->priv->missed_tree_view),
						     -1, _("Epoch"),
						     renderer, "text",
						     MISSED_EPOCH_COL, NULL);

	column = gtk_tree_view_get_column (GTK_TREE_VIEW (ui->priv->missed_tree_view), MISSED_EPOCH_COL);
	gtk_tree_view_column_set_visible (column, FALSE);

	gtk_tree_view_set_model (GTK_TREE_VIEW (ui->priv->missed_tree_view),
				 GTK_TREE_MODEL (missed_list_store));
}

static void
reminder_ui_notify_missed_reminders (ReminderUI *ui)
{
	ReminderList *missed_reminder_cntr;
	gchar *missed_reminder_text;
	guint missed_reminder_no = 0;
	GError *error = NULL;

	if ((missed_reminder_cntr = ui->priv->missed_reminders) != NULL) {
		while (missed_reminder_cntr != NULL) {
			missed_reminder_no++;
			missed_reminder_cntr = missed_reminder_cntr->next;
		}
		missed_reminder_text = g_strdup_printf (_("You have %d missed reminders"),
							missed_reminder_no);
	}

	/* notify_notification_update (ui->priv->miscellanous_notifications, */
	/* 			    _("You have missed reminders"), */
	/* 			    missed_reminder_text, */
	/* 			    NULL); */
	/* notify_notification_set_timeout (ui->priv->miscellanous_notifications, */
	/* 				 NOTIFY_EXPIRES_NEVER); */

	/* notify_notification_show (ui->priv->miscellanous_notifications, */
	/* 			  &error); */

	g_free (missed_reminder_text);
}

static void
reminder_ui_noticed_cb (ReminderUIAlarm *ui_alarm,
			ReminderUI *ui)
{
	reminder_core_set_reminder_noticed (ui->priv->reminder_core,
					    ui->priv->missed_reminders->reminder_epoch);
	reminder_list_delete_node (&(ui->priv->missed_reminders),
				   FIRST_IN_LINE,
				   0);

	/* if (!ui->priv->missed_reminders) */
	/* 	gtk_status_icon_set_blinking (ui->priv->tray_icon, */
	/* 				      FALSE); */
}

static void
reminder_ui_missed_cb (ReminderUIAlarm *ui_alarm,
		       ReminderUI *ui)
{
	gchar *time_missed_str;
	GtkTreeIter missed_iter;
	GtkListStore *missed_list_store;
	ReminderList *missed_reminders = ui->priv->missed_reminders;

	if (!missed_reminders)
		return;

	if (missed_reminders->next) {
		if (reminder_list_misc_param_exist (missed_reminders->next,
						    RU_PUSHEDOUT_PARAM)) {

			missed_reminders = missed_reminders->next;

			reminder_list_misc_param_delete (missed_reminders,
							 RU_PUSHEDOUT_PARAM);

			if (missed_reminders->next != NULL) {
				missed_list_store = (GtkListStore *) gtk_tree_view_get_model (GTK_TREE_VIEW (ui->priv->missed_tree_view));

				gtk_list_store_prepend (missed_list_store,
							&missed_iter);

				time_missed_str = reminder_ui_convert_epoch_to_pretty_string (missed_reminders->reminder_epoch,
												      MISSEDBY_STR);
				gtk_list_store_set (missed_list_store,
						    &missed_iter,
						    MISSED_TITLE_COL,
						    missed_reminders->reminder_title,
						    MISSED_SINCE_COL,
						    time_missed_str,
						    MISSED_EPOCH_COL,
						    missed_reminders->reminder_epoch,
						    -1);

				g_free (time_missed_str);
			}
			/* If there is only one node in the missed reminder list
			 * we know that the tree view widget needs to be built
			 */
			else {
				reminder_ui_build_missed_tree (ui);
			}
		}
	}
	if (reminder_list_misc_param_exist (missed_reminders,
					    RU_NOTIFPRES_PARAM)) {

		reminder_list_misc_param_delete (missed_reminders,
						 RU_NOTIFPRES_PARAM);

		/* If there is more than one node in the missed reminder list
		 * we know that the tree view widget has already been built
		 * so we just add a new entry
		 */
		if (missed_reminders->next != NULL) {
			missed_list_store = (GtkListStore *) gtk_tree_view_get_model (GTK_TREE_VIEW (ui->priv->missed_tree_view));

			gtk_list_store_prepend (missed_list_store,
						&missed_iter);

			time_missed_str = reminder_ui_convert_epoch_to_pretty_string (missed_reminders->reminder_epoch,
											      MISSEDBY_STR);
			gtk_list_store_set (missed_list_store,
					    &missed_iter,
					    MISSED_TITLE_COL,
					    missed_reminders->reminder_title,
					    MISSED_SINCE_COL,
					    time_missed_str,
					    MISSED_EPOCH_COL,
					    missed_reminders->reminder_epoch,
					    -1);

			g_free (time_missed_str);
		}
		/* If there is only one node in the missed reminder list
		 * we know that the tree view widget needs to be built
		 */
		else {
			reminder_ui_build_missed_tree (ui);
		}
	}

}

static void
reminder_ui_reminder_expired_cb (ReminderCore *core,
				 glong reminder_epoch,
				 ReminderUI *ui)
{
	GtkTreeIter iter;
	GtkTreeModel *tree_model;
	long int epoch;
	gchar *priority_str;
	gchar *title_str;
	gchar *day_str;
	gchar *time_str;
	ReminderList *exp_reminder;

	exp_reminder = reminder_core_get_reminder (ui->priv->reminder_core,
						   reminder_epoch);

	tree_model =
		gtk_tree_view_get_model (GTK_TREE_VIEW (ui->priv->current_tree_view));

	gtk_tree_model_get_iter_first (tree_model, &iter);

	if (gconf_client_get_bool (ui->priv->gconf_client,
				   REMINDER_EXPIRED_DISCARD, NULL) == FALSE)
	  {
		  gtk_tree_model_get (tree_model, &iter,
				      PRIORITY_COL, &priority_str,
				      TITLE_COL, &title_str,
				      DATE_COL, &day_str,
				      TIME_COL, &time_str,
				      EPOCH_COL, &epoch, -1);

		  gtk_list_store_remove (GTK_LIST_STORE (tree_model), &iter);

		  if (!reminder_ui_tree_has_data (ui->priv->current_tree_view))
		    {
			    g_object_ref (ui->priv->current_scrolled_window);
			    gtk_container_remove (GTK_CONTAINER (ui->priv->notebook),
						  ui->priv->current_scrolled_window);
		    }

		  tree_model =
			  gtk_tree_view_get_model (GTK_TREE_VIEW (ui->priv->expired_tree_view));

		  if (!reminder_ui_tree_has_data (ui->priv->expired_tree_view))
		    {
			    gtk_notebook_insert_page (GTK_NOTEBOOK (ui->priv->notebook),
						      ui->priv->expired_scrolled_window,
						      gtk_label_new (_("Expired Reminders")),
						      EXPIRED_REMINDER_PAGE);

			    if (gtk_widget_get_visible (ui->priv->window))
				    gtk_widget_show_all (ui->priv->expired_scrolled_window);
		    }

		  gtk_list_store_prepend (GTK_LIST_STORE (tree_model), &iter);

		  gtk_list_store_set (GTK_LIST_STORE (tree_model), &iter,
				      PRIORITY_COL, priority_str,
				      TITLE_COL, title_str,
				      DATE_COL, day_str,
				      TIME_COL, time_str,
				      EPOCH_COL, epoch, -1);

		  g_free (priority_str);
		  g_free (title_str);
		  g_free (day_str);
		  g_free (time_str);
	  }
	else
		gtk_list_store_remove (GTK_LIST_STORE (tree_model), &iter);

	/* If the most recent _missed_ reminder still has it's notification
	 * being shown, mark it as having been _pushed out_ by the currently
	 * expired reminder
	 */
	if (ui->priv->missed_reminders) {
		if (reminder_list_misc_param_exist (ui->priv->missed_reminders,
						    RU_NOTIFPRES_PARAM))
			{
				reminder_list_misc_param_delete (ui->priv->missed_reminders,
								 RU_NOTIFPRES_PARAM);
				reminder_list_misc_param_add (ui->priv->missed_reminders,
							      RU_PUSHEDOUT_PARAM,
							      NULL);
			}
	}


	reminder_list_add_node (&(ui->priv->missed_reminders),
				exp_reminder->reminder_prio,
				g_strdup (exp_reminder->reminder_title),
				exp_reminder->reminder_hour,
				exp_reminder->reminder_minute,
				exp_reminder->reminder_second,
				exp_reminder->reminder_day,
				exp_reminder->reminder_month,
				exp_reminder->reminder_year,
				exp_reminder->reminder_tz,
				exp_reminder->reminder_epoch,
				NULL,
				DESCEND_SORT);
	reminder_list_misc_param_add (ui->priv->missed_reminders,
				      RU_NOTIFPRES_PARAM,
				      NULL);

	/* gtk_status_icon_set_blinking (ui->priv->tray_icon, */
	/* 			      TRUE); */

	reminder_ui_alarm_notify (ui->priv->reminder_ui_alarm,
				  exp_reminder->reminder_title,
				  exp_reminder->reminder_prio,
				  exp_reminder->prio_det);

	reminder_list_free_node (exp_reminder);
}

static gboolean
reminder_ui_tree_has_data (GtkWidget *tree_widget)
{
	GtkTreeView *tree_view;
	GtkTreeModel *tree_model;
	GtkTreeIter tree_iter;
	gboolean ret;

	g_return_val_if_fail (tree_widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_TREE_VIEW (tree_widget), FALSE);

	tree_view = (GtkTreeView *) tree_widget;

	tree_model = gtk_tree_view_get_model (tree_view);
	ret = gtk_tree_model_get_iter_first (tree_model, &tree_iter);

	return ret;
}

static void
reminder_ui_exit_cb (GtkMenuItem *item, ReminderUI *ui)
{
	/* Hide any windows being shown and exit all the main loops */
	reminder_ui_preferences_prep_finalize (ui->priv->reminder_ui_preferences);
	reminder_ui_add_dialog_prep_finalize (ui->priv->reminder_ui_add_dialog);
	reminder_ui_alarm_prep_finalize (ui->priv->reminder_ui_alarm);
	reminder_ui_prep_finalize (ui);

	g_signal_emit (ui,
		       signals [EXIT_REQUEST_SIGNAL],
		       0);
}

static gboolean
reminder_ui_update_missed_list (GtkTreeModel *tree_model,
				GtkTreePath *path,
				GtkTreeIter *cur_iter,
				gpointer data)
{
	gchar *new_missed_str;
	glong reminder_epoch;

	gtk_tree_model_get (tree_model, cur_iter,
			    MISSED_EPOCH_COL, &reminder_epoch,
			    -1);

	new_missed_str = reminder_ui_convert_epoch_to_pretty_string (reminder_epoch,
								     MISSEDBY_STR);

	gtk_list_store_set (GTK_LIST_STORE (tree_model), cur_iter,
			    MISSED_SINCE_COL,
			    new_missed_str,
			    -1);

	g_free (new_missed_str);

	return FALSE;
}

static gboolean
reminder_ui_missed_reminder_str_update (GtkTreeView *missed_tree_view)
{
	GtkTreeModel *missed_tree_model;

	missed_tree_model = gtk_tree_view_get_model (missed_tree_view);
	gtk_tree_model_foreach (missed_tree_model,
				reminder_ui_update_missed_list,
				NULL);

	return TRUE;
}

static gboolean
reminder_ui_show_missed_reminders (ReminderUI *ui)
{
	GtkWidget *dialog,
		*missed_scrolled_view;
	guint update_timeout_id;
	GSource *update_timeout_src;

	dialog = gtk_dialog_new_with_buttons (_("Missed Reminders"),
					      NULL,
					      GTK_DIALOG_MODAL,
					      GTK_STOCK_OK,
					      GTK_RESPONSE_ACCEPT,
					      NULL);
	gtk_window_set_skip_taskbar_hint (GTK_WINDOW (dialog),
					  TRUE);

	missed_scrolled_view = gtk_scrolled_window_new (NULL, NULL);

	gtk_container_add (GTK_CONTAINER (missed_scrolled_view),
			   ui->priv->missed_tree_view);
	gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (dialog))),
			    missed_scrolled_view,
			    TRUE,
			    TRUE,
			    6);

	gtk_widget_set_size_request (dialog,
				     400, 300);

	/* Update the time-sinced-missed field in the tree view widget before showing it */
	reminder_ui_missed_reminder_str_update ((gpointer) ui->priv->missed_tree_view);

	gtk_widget_show_all (missed_scrolled_view);
	update_timeout_id = g_timeout_add_seconds (1,
						   (GSourceFunc) reminder_ui_missed_reminder_str_update,
						   ui->priv->missed_tree_view);

	gtk_dialog_run (GTK_DIALOG (dialog));

	update_timeout_src = g_main_context_find_source_by_id (g_main_context_default (),
							       update_timeout_id);
	g_source_destroy (update_timeout_src);

	gtk_widget_destroy (dialog);

	reminder_core_set_reminder_noticed (ui->priv->reminder_core, -1);
	/* gtk_status_icon_set_blinking (ui->priv->tray_icon, */
	/* 			      FALSE); */
	reminder_list_delete_node (&(ui->priv->missed_reminders),
				   DELETE_ALL,
				   0);
	ui->priv->missed_tree_view = NULL;

	return FALSE;
}

static void
reminder_ui_add_to_tree (GtkTreeView *tree_view, ReminderList *new_reminder)
{
	GtkTreeModel *tree_model;
	GtkTreeIter tree_iter, temp_iter, new_iter;
	long int epoch_time;
	gchar *date_str, *time_str;

	g_return_if_fail (tree_view != NULL);
	g_return_if_fail (GTK_IS_TREE_VIEW (tree_view));
	g_return_if_fail (new_reminder != NULL);

	date_str = g_strdup_printf ("%d/%d/%d",
				    new_reminder->reminder_day,
				    new_reminder->reminder_month,
				    new_reminder->reminder_year);
	time_str = g_strdup_printf ("%d:%d:%d",
				    new_reminder->reminder_hour,
				    new_reminder->reminder_minute,
				    new_reminder->reminder_second);

	tree_model = gtk_tree_view_get_model (tree_view);

	if (gtk_tree_model_get_iter_first (tree_model, &tree_iter))
	  {
		  do
		    {
			    gtk_tree_model_get (tree_model, &tree_iter,
						EPOCH_COL, &epoch_time,
						-1);

			    /* Have a copy of the present iter so that we have a reference on the last
			     * row in the model after the copied iter has been made invalid
			     */
			    temp_iter = tree_iter;

			    if (epoch_time > new_reminder->reminder_epoch)
			      {
				      gtk_list_store_insert_before (GTK_LIST_STORE (tree_model),
								    &new_iter, &tree_iter);
				      break;
			      }
		    }
		  while (gtk_tree_model_iter_next (tree_model, &tree_iter));

		  if (!gtk_tree_model_iter_next (tree_model, &temp_iter)
		      && epoch_time < new_reminder->reminder_epoch)
			  gtk_list_store_append (GTK_LIST_STORE (tree_model),
						 &new_iter);
	  }
	else
		gtk_list_store_append (GTK_LIST_STORE (tree_model),
				       &new_iter);

	gtk_list_store_set (GTK_LIST_STORE (tree_model),
			    &new_iter,
			    PRIORITY_COL, reminder_common_get_priority_icon_name (new_reminder->reminder_prio),
			    TITLE_COL, new_reminder->reminder_title,
			    DATE_COL, date_str,
			    TIME_COL, time_str,
			    EPOCH_COL, new_reminder->reminder_epoch,
			    -1);

	g_free (date_str);
	g_free (time_str);
}

static gboolean
reminder_ui_fill_epoch_list (GtkTreeModel *tree_model,
			     GtkTreePath *current_path,
			     GtkTreeIter *current_iter,
			     gpointer list_ptr)
{
	GList **reminder_epoch_list = (GList **) list_ptr;
	gint *reminder_epoch;

	reminder_epoch = (gint *) g_malloc (sizeof (gint));

	gtk_tree_model_get (tree_model, current_iter,
			    EPOCH_COL, reminder_epoch,
			    -1);

	*reminder_epoch_list = g_list_insert (*reminder_epoch_list,
					      reminder_epoch,
					      -1);

	return FALSE;
}

static void
reminder_ui_new_reminder_cb (GtkMenuItem *menu_item, ReminderUI *ui)
{
	ReminderList *new_reminder = NULL;
	GtkTreeModel *tree_model;
	GList *current_reminder_list = NULL;

	tree_model = gtk_tree_view_get_model (GTK_TREE_VIEW (ui->priv->current_tree_view));

	/* Get a list of all the reminder epochs in the current tree view and put them
	 * in the GList so that they may be checked by the add dialog to prevent
	 * reminders with duplicate epochs from being added
	 */
	gtk_tree_model_foreach (tree_model,
				reminder_ui_fill_epoch_list,
				&current_reminder_list);

	new_reminder =
		reminder_ui_add_dialog_show (ui->priv->reminder_ui_add_dialog,
					     NULL,
					     current_reminder_list);

	if (!new_reminder)
		goto out;

	if (!reminder_ui_tree_has_data (ui->priv->current_tree_view))
		{
			gtk_notebook_insert_page (GTK_NOTEBOOK (ui->priv->notebook),
						  ui->priv->current_scrolled_window,
						  gtk_label_new (_("Current Reminders")),
						  CURRENT_REMINDER_PAGE);

			if (gtk_widget_get_visible (ui->priv->window))
				gtk_widget_show_all (ui->priv->current_scrolled_window);
		}

	reminder_core_add_reminder (ui->priv->reminder_core,
				    new_reminder,
				    CURRENT_REMINDER);

	reminder_ui_add_to_tree (GTK_TREE_VIEW (ui->priv->current_tree_view),
				 new_reminder);

	reminder_list_free_node (new_reminder);

 out:
	g_list_foreach (current_reminder_list,
			(GFunc) g_free,
			NULL);
	g_list_free (current_reminder_list);
}

static void
reminder_ui_quick_add_reminder_cb (GtkMenuItem *menu_item, ReminderUI *ui)
{
	ReminderUIAddDialogQuickAddType quick_add_type;
	ReminderList *new_reminder;
	GtkTreeModel *tree_model;
	GList *current_reminder_list = NULL;

	tree_model = gtk_tree_view_get_model (GTK_TREE_VIEW (ui->priv->current_tree_view));

	/* Get a list of all the reminder epochs in the current tree view and put them
	 * in the GList so that they may be checked by the add dialog to prevent
	 * reminders with duplicate epochs from being added
	 */
	gtk_tree_model_foreach (tree_model,
				reminder_ui_fill_epoch_list,
				&current_reminder_list);

	quick_add_type =
		(ReminderUIAddDialogQuickAddType) atoi (gtk_widget_get_name ((GtkWidget *) menu_item));

	new_reminder =
		reminder_ui_add_dialog_quick_add_reminder (ui->priv->reminder_ui_add_dialog,
							   quick_add_type,
							   current_reminder_list);

	if (!new_reminder)
		goto out;

	else {
		if (!reminder_ui_tree_has_data (ui->priv->current_tree_view)) {
			gtk_notebook_insert_page (GTK_NOTEBOOK (ui->priv->notebook),
						  ui->priv->current_scrolled_window,
						  gtk_label_new (_("Current Reminders")),
						  CURRENT_REMINDER_PAGE);

			if (gtk_widget_get_visible (ui->priv->window))
				gtk_widget_show_all (ui->priv->current_scrolled_window);
		}
		reminder_ui_add_to_tree (GTK_TREE_VIEW (ui->priv->current_tree_view),
					 new_reminder);

		reminder_core_add_reminder (ui->priv->reminder_core,
					    new_reminder,
					    CURRENT_REMINDER);
	  }
	reminder_list_free_node (new_reminder);

 out:
	g_list_foreach (current_reminder_list,
			(GFunc) g_free,
			NULL);
	g_list_free (current_reminder_list);
}

static void
reminder_ui_show_preferences_cb (GtkMenuItem *item, ReminderUI *ui)
{
	reminder_ui_preferences_show (ui->priv->reminder_ui_preferences);

	if (gconf_client_get_bool (ui->priv->gconf_client,
				   REMINDER_EXPIRED_DISCARD,
				   NULL) == TRUE &&
	    reminder_ui_tree_has_data (ui->priv->expired_tree_view))
		{
			if (reminder_ui_notice_dialog_show_confirm (GTK_WINDOW (ui->priv->window),
								    DELETE_EXPIRED_REMINDERS) == TRUE) {
				reminder_core_purge_reminders (ui->priv->reminder_core,
							       EXPIRED_REMINDER);
				gtk_list_store_clear (GTK_LIST_STORE (gtk_tree_view_get_model (GTK_TREE_VIEW (ui->priv->expired_tree_view))));
				g_object_ref (ui->priv->expired_scrolled_window);
				gtk_container_remove (GTK_CONTAINER (ui->priv->notebook),
						      ui->priv->expired_scrolled_window);

			}
		}

}

static void
reminder_ui_popup_menu_cleared_cb (GtkMenu *menu)
{
	g_object_ref_sink (menu);
	g_object_unref (menu);
}

static void
reminder_ui_popup_menu_cb (GtkStatusIcon *status_icon, guint button,
			   guint activate_time, ReminderUI *ui)
{
	GtkWidget *menu, *quick_add_menu, *menu_item;

	menu = gtk_menu_new ();

	menu_item = gtk_image_menu_item_new_from_stock (GTK_STOCK_QUIT, NULL);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);
	g_signal_connect (menu_item, "activate",
			  G_CALLBACK (reminder_ui_exit_cb), ui);

	menu_item =
		gtk_image_menu_item_new_from_stock (GTK_STOCK_PREFERENCES,
						    NULL);
	gtk_menu_shell_prepend (GTK_MENU_SHELL (menu), menu_item);
	g_signal_connect (menu_item, "activate",
			  G_CALLBACK (reminder_ui_show_preferences_cb), ui);

	menu_item = gtk_separator_menu_item_new ();
	gtk_menu_shell_prepend (GTK_MENU_SHELL (menu), menu_item);

	menu_item = gtk_image_menu_item_new_with_label (_("New Reminder"));
	gtk_menu_shell_prepend (GTK_MENU_SHELL (menu), menu_item);
	g_signal_connect (menu_item, "activate",
			  G_CALLBACK (reminder_ui_new_reminder_cb), ui);

	menu_item = gtk_image_menu_item_new_with_label (_("Quick Reminder"));
	gtk_menu_shell_prepend (GTK_MENU_SHELL (menu), menu_item);

	g_signal_connect (menu, "hide",
			  G_CALLBACK (reminder_ui_popup_menu_cleared_cb),
			  NULL);

	quick_add_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menu_item), quick_add_menu);

	menu_item = gtk_image_menu_item_new_with_label (_("5 Minutes"));
	gtk_widget_set_name (menu_item,
			     g_strdup_printf ("%d", FIVE_MINUTE_QUICK_ADD));
	gtk_menu_shell_append (GTK_MENU_SHELL (quick_add_menu), menu_item);
	g_signal_connect (menu_item, "activate",
			  G_CALLBACK (reminder_ui_quick_add_reminder_cb), ui);

	menu_item = gtk_image_menu_item_new_with_label (_("10 Minutes"));
	gtk_widget_set_name (menu_item,
			     g_strdup_printf ("%d", TEN_MINUTE_QUICK_ADD));
	gtk_menu_shell_append (GTK_MENU_SHELL (quick_add_menu), menu_item);
	g_signal_connect (menu_item, "activate",
			  G_CALLBACK (reminder_ui_quick_add_reminder_cb), ui);

	menu_item = gtk_image_menu_item_new_with_label (_("15 Minutes"));
	gtk_widget_set_name (menu_item,
			     g_strdup_printf ("%d",
					      FIFTEEN_MINUTE_QUICK_ADD));
	gtk_menu_shell_append (GTK_MENU_SHELL (quick_add_menu), menu_item);
	g_signal_connect (menu_item, "activate",
			  G_CALLBACK (reminder_ui_quick_add_reminder_cb), ui);

	menu_item = gtk_image_menu_item_new_with_label (_("30 Minutes"));
	gtk_widget_set_name (menu_item,
			     g_strdup_printf ("%d", THIRTY_MINUTE_QUICK_ADD));
	gtk_menu_shell_append (GTK_MENU_SHELL (quick_add_menu), menu_item);
	g_signal_connect (menu_item, "activate",
			  G_CALLBACK (reminder_ui_quick_add_reminder_cb), ui);

	menu_item = gtk_image_menu_item_new_with_label (_("60 Minutes"));
	gtk_widget_set_name (menu_item,
			     g_strdup_printf ("%d", SIXTY_MINUTE_QUICK_ADD));
	gtk_menu_shell_append (GTK_MENU_SHELL (quick_add_menu), menu_item);
	g_signal_connect (menu_item, "activate",
			  G_CALLBACK (reminder_ui_quick_add_reminder_cb), ui);

	gtk_widget_show_all (menu);
	gtk_menu_popup (GTK_MENU (menu),
			NULL,
			NULL,
			gtk_status_icon_position_menu,
			status_icon, button, activate_time);
}

static void
reminder_ui_add_reminder_cb (GtkToolButton *tool_button, ReminderUI *ui)
{
	ReminderList *new_reminder = NULL;
	GList *current_reminder_list = NULL;
	GtkTreeModel *tree_model;

	tree_model = gtk_tree_view_get_model (GTK_TREE_VIEW (ui->priv->current_tree_view));

	/* Get a list of all the reminder epochs in the current tree view and put them
	 * in the GList so that they may be checked by the add dialog to prevent
	 * reminders with duplicate epochs from being added
	 */
	gtk_tree_model_foreach (tree_model,
				reminder_ui_fill_epoch_list,
				&current_reminder_list);

	new_reminder =
		reminder_ui_add_dialog_show (ui->priv->reminder_ui_add_dialog,
					     NULL,
					     current_reminder_list);

	if (!new_reminder)
		goto out;

	reminder_core_add_reminder (ui->priv->reminder_core,
				    new_reminder,
				    CURRENT_REMINDER);

	if (!reminder_ui_tree_has_data (ui->priv->current_tree_view)) {

		gtk_notebook_insert_page (GTK_NOTEBOOK (ui->priv->notebook),
					  ui->priv->current_scrolled_window,
					  gtk_label_new (_("Current Reminders")),
					  CURRENT_REMINDER_PAGE);

		if (!gtk_widget_get_visible (ui->priv->current_scrolled_window))
			gtk_widget_show_all (ui->priv->current_scrolled_window);
	}

	reminder_ui_add_to_tree (GTK_TREE_VIEW (ui->priv->current_tree_view),
				 new_reminder);

	reminder_list_free_node (new_reminder);

 out:
	g_list_foreach (current_reminder_list,
			(GFunc) g_free,
			NULL);
	g_list_free (current_reminder_list);
}

static void
reminder_ui_edit_reminder_cb (GtkToolButton *tool_button, ReminderUI *ui)
{
	GtkWidget *tree_view, *tree_scrolled_bin;
	GtkTreeSelection *selection;
	GtkTreeModel *tree_model;
	GtkTreeIter iter, temp_iter;
	long int epoch;
	ReminderList *old_reminder;
	ReminderList *new_reminder;
	gchar *date_str, *time_str;
	gboolean list_insert_done = FALSE;
	GList *current_reminder_list = NULL;

	tree_scrolled_bin =
		gtk_notebook_get_nth_page (GTK_NOTEBOOK (ui->priv->notebook),
					   gtk_notebook_get_current_page (GTK_NOTEBOOK (ui->priv->notebook)));

	tree_view = gtk_bin_get_child (GTK_BIN (tree_scrolled_bin));

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));

	if (!gtk_tree_selection_get_selected (selection, NULL, &iter))
		return;

	tree_model = gtk_tree_view_get_model (GTK_TREE_VIEW (tree_view));

	/* Get a list of all the reminder epochs in the current tree view and put them
	 * in the GList so that they may be checked by the add dialog to prevent
	 * reminders with duplicate epochs from being added
	 */
	gtk_tree_model_foreach (tree_model,
				reminder_ui_fill_epoch_list,
				&current_reminder_list);

	gtk_tree_model_get (tree_model, &iter,
			    EPOCH_COL, &epoch,
			    -1);

	old_reminder = reminder_core_get_reminder (ui->priv->reminder_core, epoch);

	if (!old_reminder) {
		g_warning (_("The reminder shown in the user interface does not seem to\n"
			     "be present in the core, this could be a serious problem.\n"
			     "Please file a bug report on the Reminder-ng bug tracker\n"));
		return;
	}
	new_reminder =
		reminder_ui_add_dialog_show (ui->priv->reminder_ui_add_dialog,
					     old_reminder,
					     current_reminder_list);

	if (!new_reminder) {
		reminder_list_free_node (old_reminder);
		return;
	}

	if (gtk_notebook_get_current_page (GTK_NOTEBOOK (ui->priv->notebook))
	    == CURRENT_REMINDER_PAGE)
		{
			reminder_core_delete_reminder (ui->priv->reminder_core,
						       epoch,
						       CURRENT_REMINDER);
			reminder_core_add_reminder (ui->priv->reminder_core,
						    new_reminder,
						    CURRENT_REMINDER);
		}
	else
		{
			reminder_core_delete_reminder (ui->priv->reminder_core,
						       epoch,
						       EXPIRED_REMINDER);
			/* Obviously the user has recycled the reminder, so add
			 * it to the current reminder list
			 */
			reminder_core_add_reminder (ui->priv->reminder_core,
						    new_reminder,
						    CURRENT_REMINDER);


		}

	if (epoch != new_reminder->reminder_epoch)
	  {

		  /* If the user is recycling an expired reminder */
		  if (tree_view == ui->priv->expired_tree_view) {
			  gtk_list_store_remove (GTK_LIST_STORE (tree_model), &iter);

			  if (!reminder_ui_tree_has_data (ui->priv->expired_tree_view))
				  {
					  g_object_ref (tree_scrolled_bin);
					  gtk_container_remove (GTK_CONTAINER (ui->priv->notebook),
								tree_scrolled_bin);
				  }

			  /* Switch to the current tree model as that's where the reminder is supposed to go */
			  tree_model = gtk_tree_view_get_model (GTK_TREE_VIEW (ui->priv->current_tree_view));
		  }
		  else
			  gtk_list_store_remove (GTK_LIST_STORE (tree_model), &iter);

		  if (gtk_tree_model_get_iter_first (tree_model, &iter))
		    {

			    do
			      {
				      temp_iter = iter;

				      gtk_tree_model_get (tree_model,
							  &iter,
							  EPOCH_COL,
							  &epoch, -1);

				      if (epoch > new_reminder->reminder_epoch)
					{
						gtk_list_store_insert_before (GTK_LIST_STORE (tree_model),
									      &iter,
									      &temp_iter);

						list_insert_done = TRUE;
						break;
					}

			      }
			    while (gtk_tree_model_iter_next (tree_model, &iter));

			    if (!gtk_tree_model_iter_next (tree_model, &temp_iter)
				&& list_insert_done == FALSE)
				    gtk_list_store_append (GTK_LIST_STORE
							   (tree_model),
							   &iter);

		    }
		  else
			  gtk_list_store_prepend (GTK_LIST_STORE (tree_model),
						  &iter);


	  }

	date_str = g_strdup_printf ("%d/%d/%d",
				    new_reminder->reminder_day,
				    new_reminder->reminder_month,
				    new_reminder->reminder_year);
	time_str = g_strdup_printf ("%d:%d:%d",
				    new_reminder->reminder_hour,
				    new_reminder->reminder_minute,
				    new_reminder->reminder_second);

	gtk_list_store_set (GTK_LIST_STORE (tree_model),
			    &iter,
			    PRIORITY_COL, reminder_common_get_priority_icon_name (new_reminder->reminder_prio),
			    TITLE_COL, new_reminder->reminder_title,
			    DATE_COL, date_str,
			    TIME_COL, time_str,
			    EPOCH_COL, new_reminder->reminder_epoch,
			    -1);

	reminder_list_free_node (new_reminder);
	reminder_list_free_node (old_reminder);
	g_free (date_str);
	g_free (time_str);
	g_list_foreach (current_reminder_list,
			(GFunc) g_free,
			NULL);
	g_list_free (current_reminder_list);
}

static void
reminder_ui_delete_reminder_cb (GtkToolButton *tool_button, ReminderUI *ui)
{
	GtkWidget *tree_view;
	GtkTreeSelection *selection;
	GtkListStore *list_store;
	GtkTreeIter iter, next_iter;
	GtkWidget *tree_scrolled_bin;
	long int epoch;

	tree_scrolled_bin =
		gtk_notebook_get_nth_page (GTK_NOTEBOOK (ui->priv->notebook),
					   gtk_notebook_get_current_page
					   (GTK_NOTEBOOK
					    (ui->priv->notebook)));

	tree_view = gtk_bin_get_child (GTK_BIN (tree_scrolled_bin));

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));

	if (!gtk_tree_selection_get_selected (selection, NULL, &iter))
		return;

	list_store = (GtkListStore *) gtk_tree_view_get_model (GTK_TREE_VIEW (tree_view));

	gtk_tree_model_get (GTK_TREE_MODEL (list_store), &iter,
			    EPOCH_COL, &epoch,
			    -1);

	gtk_tree_model_get_iter_first (GTK_TREE_MODEL (list_store),
				       &next_iter);

	if (!gtk_list_store_remove (list_store, &iter)
	    && reminder_ui_tree_has_data (tree_view))
	  {
		  gtk_list_store_move_before (list_store, &next_iter, NULL);

	  }
	else
		next_iter = iter;

	/* Check what tree we are using by comparing where the pointer
	 * actually points to
	 */
	if (tree_view == ui->priv->current_tree_view)
		reminder_core_delete_reminder (ui->priv->reminder_core, epoch,
					   CURRENT_REMINDER);
	else if (tree_view == ui->priv->expired_tree_view)
		reminder_core_delete_reminder (ui->priv->reminder_core, epoch,
					       EXPIRED_REMINDER);

	if (!reminder_ui_tree_has_data (tree_view))
	  {
		  g_object_ref (tree_scrolled_bin);
		  gtk_container_remove (GTK_CONTAINER (ui->priv->notebook),
					tree_scrolled_bin);
	  }
	else
		gtk_tree_selection_select_iter (selection, &next_iter);

}

/* Is this really necessary? */
/* static void */
/* reminder_ui_find_reminder_cb (GtkToolButton *button, ReminderUI *ui) */
/* { */


/* } */

static void
reminder_ui_show_dialog_cb (GtkStatusIcon *status_icon, ReminderUI *ui)
{
	GError *error = NULL;

	if (gtk_widget_get_visible (ui->priv->window) && ui->priv->missed_tree_view != NULL)
		{
			if (!gtk_widget_get_visible (ui->priv->missed_tree_view))
				reminder_ui_show_missed_reminders (ui);
			else
				return;
		}
	else if (gtk_widget_get_visible (ui->priv->window)) {
		gtk_widget_hide (ui->priv->window);

		g_main_loop_quit (ui->priv->main_window_loop);
	}

	else {
		gtk_widget_show_all (ui->priv->window);

		if (ui->priv->missed_tree_view != NULL) {
			/* Hide the notification since it has obviously served it's
			 * purpose
			 */
			/* notify_notification_close (ui->priv->miscellanous_notifications, */
			/* 			   &error); */
			/* Disregard errors as they would be since the reminders are not present */
			if (error) {
				g_error_free (error);
				error = NULL;
			}
			reminder_ui_show_missed_reminders (ui);
		}

		g_main_loop_run (ui->priv->main_window_loop);
	}
}

static gchar *
reminder_ui_convert_epoch_to_pretty_string (gint reminder_epoch, ReminderUIPrtyStrType str_type)
{
	gchar *pretty_string, *pretty_string_tmp;
	GTimeVal current_time;
	gint time_to_expire, hours, minutes, seconds;

	g_get_current_time (&current_time);

	switch (str_type) {
	case COUNTDOWN_STR:
		time_to_expire = reminder_epoch - current_time.tv_sec;
		break;

	case MISSEDBY_STR:
		time_to_expire = current_time.tv_sec - reminder_epoch;
		break;

	default:
		g_warning ("Wrong str_type specified");
		return NULL;
	}



	if ((time_to_expire / (60 * 60)) > 24)
		{
			pretty_string = g_strdup_printf (_("%d Days"),
							 time_to_expire / (60 * 60 * 24));
		}
	else if ((time_to_expire / (60 * 60)) > 0)
		{
			hours = time_to_expire / (60 * 60);
			minutes = (time_to_expire - (hours * 60 * 60)) / 60;
			pretty_string =
				g_strdup_printf (_("%d Hours and %d Minutes"), hours,
						 minutes);
		}
	else if ((time_to_expire / 60) > 0)
		{
			minutes = time_to_expire / 60;
			seconds = (time_to_expire - (minutes * 60));
			pretty_string =
				g_strdup_printf (_("%d Minutes and %d Seconds"),
						 minutes, seconds);
		}
	else
		{
			pretty_string =
				g_strdup_printf (_("%d Seconds"),
						 time_to_expire);
		}
	switch (str_type) {
	case MISSEDBY_STR:
		pretty_string_tmp = pretty_string;

		/* As it gives the time since the reminder was missed,
		 * an extra word is concatenated
		 */
		pretty_string = g_strconcat (pretty_string_tmp,
					     _(" Ago"),
					     NULL);

		g_free (pretty_string_tmp);
		break;

	case COUNTDOWN_STR:
	default:
		break;
	}

	return pretty_string;
}

static gboolean
reminder_ui_attach_tooltip_to_tray_icon (gpointer data)
{
	gint reminder_epoch_current;
	gchar *tooltip_text,
		*reminder_title,
		*reminder_title_temp,
		*countdown_string,
		*missed_reminder_text = NULL;
	GtkTreeIter first_reminder;
	GtkTreeModel *current_reminder_list;
	guint missed_reminder_no = 0;
	ReminderUI *ui = REMINDER_UI (data);
	ReminderList *missed_reminder_cntr;

	if (ui->priv->missed_reminders != NULL) {
		if (reminder_list_misc_param_exist (ui->priv->missed_reminders,
						    RU_NOTIFPRES_PARAM))
			missed_reminder_cntr = ui->priv->missed_reminders->next;
		else
			missed_reminder_cntr = ui->priv->missed_reminders;

		if (missed_reminder_cntr != NULL) {
			while (missed_reminder_cntr != NULL) {
				missed_reminder_no++;
				missed_reminder_cntr = missed_reminder_cntr->next;
			}
			missed_reminder_text = g_strdup_printf (_("\nMissed reminders: %d"),
								missed_reminder_no);
		}
	}
	if (reminder_ui_tree_has_data (ui->priv->current_tree_view))
	  {

		  current_reminder_list =
			  gtk_tree_view_get_model (GTK_TREE_VIEW (ui->priv->current_tree_view));
		  gtk_tree_model_get_iter_first (current_reminder_list,
						 &first_reminder);
		  gtk_tree_model_get (current_reminder_list, &first_reminder,
				      TITLE_COL, &reminder_title,
				      EPOCH_COL, &reminder_epoch_current,
				      -1);

		  countdown_string = reminder_ui_convert_epoch_to_pretty_string (reminder_epoch_current,
										 COUNTDOWN_STR);

		  /* Limit the amount of characters, otherwise there is the chance of getting really
		   * enourmous tooltips which the user probably won't like
		   */
		  if (g_utf8_strlen (reminder_title, -1) > 15)
		    {
			    reminder_title_temp =
				    g_strdup_printf ("%.6s...%.6s",
						     reminder_title,
						     (reminder_title + (g_utf8_strlen (reminder_title, -1) - 6)));

			    g_free (reminder_title);

			    reminder_title = reminder_title_temp;
		    }

		  tooltip_text =
			  g_markup_printf_escaped (_("<b>The current reminder is \"%s\"</b>\n"
						     "Which alarms in %s"
						     "<b>%s</b>"),
						   reminder_title,
						   countdown_string,
						   missed_reminder_no ? missed_reminder_text : "");

		  gtk_status_icon_set_tooltip_markup (ui->priv->tray_icon,
						      tooltip_text);

		  g_free (countdown_string);
		  g_free (reminder_title);
	  }
	else
	  {
		  tooltip_text = g_strdup_printf (_("No current reminders"));


		  gtk_status_icon_set_tooltip_text (ui->priv->tray_icon,
						    tooltip_text);
	  }

	if (missed_reminder_text)
		g_free (missed_reminder_text);
	g_free (tooltip_text);

	return TRUE;
}

static gboolean
reminder_ui_closed_cb (GtkWidget *window,
		       GdkEvent *event,
		       ReminderUI *ui)
{
	gtk_widget_hide (window);
	g_main_loop_quit (ui->priv->main_window_loop);

	return TRUE;
}

static void
reminder_ui_prep_finalize (ReminderUI *ui)
{
	if (gtk_widget_get_visible (ui->priv->window)) {
		gtk_widget_hide (ui->priv->window);
		g_main_loop_quit (ui->priv->main_window_loop);
	}
}

static void
reminder_ui_finalize (GObject *object)
{
	ReminderUI *ui;

	g_return_if_fail (object != NULL);
	g_return_if_fail (REMINDER_IS_UI (object));

	ui = REMINDER_UI (object);

	if (ui->priv->missed_reminders) {
		if (!reminder_list_misc_param_exist (ui->priv->missed_reminders,
						     RU_NOTIFPRES_PARAM))
			{
				g_object_unref (gtk_tree_view_get_model (GTK_TREE_VIEW (ui->priv->missed_tree_view)));
				gtk_widget_destroy (ui->priv->missed_tree_view);
			}
		else if (ui->priv->missed_reminders->next) {
			g_object_unref (gtk_tree_view_get_model (GTK_TREE_VIEW (ui->priv->missed_tree_view)));
			gtk_widget_destroy (ui->priv->missed_tree_view);
		}

		reminder_list_delete_node (&(ui->priv->missed_reminders),
					   DELETE_ALL,
					   0);
	}

	g_object_unref (ui->priv->reminder_ui_add_dialog);
	g_object_unref (ui->priv->reminder_ui_preferences);
	g_object_unref (ui->priv->reminder_ui_alarm);
	g_object_unref (ui->priv->tray_icon);
	/* g_object_unref (ui->priv->miscellanous_notifications); */
	g_object_unref (ui->priv->gconf_client);
	g_main_loop_unref (ui->priv->main_window_loop);

	gtk_widget_destroy (ui->priv->window);
}

static void
reminder_ui_class_init (ReminderUIClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = reminder_ui_finalize;

	signals [EXIT_REQUEST_SIGNAL] =
		g_signal_new ("exit-request",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      0,
			      NULL,
			      NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE,
			      0);

	g_type_class_add_private (klass, sizeof (ReminderUIPrivate));
}

static void
reminder_ui_init (ReminderUI *ui)
{
	GtkWidget *toolbutton;

	/* notify_init (_("Reminder-ng")); */

	ui->priv = REMINDER_UI_GET_PRIVATE (ui);

	ui->priv->missed_reminders = NULL;
	ui->priv->missed_tree_view = NULL;

	ui->priv->main_window_loop = g_main_loop_new (NULL,
						      FALSE);

	ui->priv->tray_icon =
		gtk_status_icon_new_from_icon_name ("reminder-ng");

	ui->priv->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (ui->priv->window),
			      _("Reminder-ng"));
	gtk_window_set_icon_name (GTK_WINDOW (ui->priv->window),
				  REMINDER_NG_APP_ICON);

	ui->priv->vbox_main = gtk_vbox_new (FALSE, 8);
	ui->priv->toolbar = gtk_toolbar_new ();
	ui->priv->notebook = gtk_notebook_new ();

	/* Buttons in the toolbar */
	toolbutton =
		(GtkWidget *) gtk_tool_button_new_from_stock (GTK_STOCK_NEW);
	gtk_toolbar_insert (GTK_TOOLBAR (ui->priv->toolbar),
			    GTK_TOOL_ITEM (toolbutton), -1);
	gtk_widget_set_tooltip_text (toolbutton, _("New reminder"));
	g_signal_connect (toolbutton, "clicked",
			  G_CALLBACK (reminder_ui_add_reminder_cb), ui);

	toolbutton =
		(GtkWidget *) gtk_tool_button_new_from_stock (GTK_STOCK_EDIT);
	gtk_toolbar_insert (GTK_TOOLBAR (ui->priv->toolbar),
			    GTK_TOOL_ITEM (toolbutton), -1);
	gtk_widget_set_tooltip_text (toolbutton, _("Edit reminder"));
	g_signal_connect (toolbutton, "clicked",
			  G_CALLBACK (reminder_ui_edit_reminder_cb), ui);

	/* Is a find button necessary in lieu of the tree view's inbuilt
	 * search feature?
	 */
	/* toolbutton = */
	/* 	(GtkWidget *) gtk_tool_button_new_from_stock (GTK_STOCK_FIND); */
	/* gtk_toolbar_insert (GTK_TOOLBAR (ui->priv->toolbar), */
	/* 		    GTK_TOOL_ITEM (toolbutton), -1); */
	/* gtk_widget_set_tooltip_text (toolbutton, _("Find reminder")); */
	/* g_signal_connect (toolbutton, "clicked", */
	/* 		  G_CALLBACK (reminder_ui_find_reminder_cb), ui); */

	toolbutton =
		(GtkWidget *)
		gtk_tool_button_new_from_stock (GTK_STOCK_DELETE);
	gtk_toolbar_insert (GTK_TOOLBAR (ui->priv->toolbar),
			    GTK_TOOL_ITEM (toolbutton), -1);
	gtk_widget_set_tooltip_text (toolbutton, _("Delete reminder"));
	g_signal_connect (toolbutton, "clicked",
			  G_CALLBACK (reminder_ui_delete_reminder_cb), ui);

	gtk_toolbar_insert (GTK_TOOLBAR (ui->priv->toolbar),
			    gtk_separator_tool_item_new (), -1);

	toolbutton =
		(GtkWidget *)
		gtk_tool_button_new_from_stock (GTK_STOCK_PREFERENCES);
	gtk_toolbar_insert (GTK_TOOLBAR (ui->priv->toolbar),
			    GTK_TOOL_ITEM (toolbutton), -1);
	g_signal_connect (toolbutton, "clicked",
			  G_CALLBACK (reminder_ui_show_preferences_cb), ui);
	gtk_widget_set_tooltip_text (toolbutton, _("Set user preferences"));
	/* End of toolbar buttons */

	/* Tree view widgets to display the reminders and their details */
	ui->priv->current_tree_view = gtk_tree_view_new ();
	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (ui->priv->current_tree_view),
				      TRUE);

	ui->priv->current_scrolled_window = gtk_scrolled_window_new (NULL,
								     NULL);
	gtk_container_add (GTK_CONTAINER (ui->priv->current_scrolled_window),
			   ui->priv->current_tree_view);

	ui->priv->expired_tree_view = gtk_tree_view_new ();
	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (ui->priv->expired_tree_view),
				      TRUE);

	ui->priv->expired_scrolled_window = gtk_scrolled_window_new (NULL,
								       NULL);
	gtk_container_add (GTK_CONTAINER (ui->priv->expired_scrolled_window),
			   ui->priv->expired_tree_view);
	/* End of tree view widgets related to reminders */

	gtk_box_pack_start (GTK_BOX (ui->priv->vbox_main), ui->priv->toolbar,
			    FALSE, FALSE, 6);

	gtk_box_pack_start (GTK_BOX (ui->priv->vbox_main), ui->priv->notebook,
			    TRUE, TRUE, 6);

	gtk_widget_set_size_request (ui->priv->notebook, 300, 300);
	gtk_widget_set_size_request (ui->priv->toolbar, -1, 40);
	gtk_container_add (GTK_CONTAINER (ui->priv->window),
			   ui->priv->vbox_main);
	gtk_widget_set_size_request (ui->priv->window,
				     370, 300);

	ui->priv->reminder_ui_add_dialog = reminder_ui_add_dialog_new ();
	ui->priv->reminder_ui_preferences = reminder_ui_preferences_new ();

	/* Initialise alarms & notifications */
	ui->priv->reminder_ui_alarm = reminder_ui_alarm_new ();

	/* ui->priv->miscellanous_notifications = notify_notification_new ("Misc Notifications", */
	/* 								NULL, */
	/* 								NULL, */
	/* 								NULL); */
	/* End of alarms & notifications initialisation */

	ui->priv->gconf_client = gconf_client_get_default ();

	g_timeout_add_seconds (1,
			       reminder_ui_attach_tooltip_to_tray_icon,
			       ui);

	g_signal_connect (ui->priv->tray_icon, "activate",
			  G_CALLBACK (reminder_ui_show_dialog_cb), ui);
	g_signal_connect (ui->priv->tray_icon, "popup_menu",
			  G_CALLBACK (reminder_ui_popup_menu_cb), ui);
	g_signal_connect (ui->priv->window, "delete-event",
			  G_CALLBACK (reminder_ui_closed_cb), ui);

	g_signal_connect (ui->priv->reminder_ui_alarm,
			  "reminder-noticed",
			  G_CALLBACK (reminder_ui_noticed_cb),
			  ui);
	g_signal_connect (ui->priv->reminder_ui_alarm,
			  "reminder-missed",
			  G_CALLBACK (reminder_ui_missed_cb),
			  ui);

}

ReminderUI *
reminder_ui_new ()
{
	ReminderUI *ui = g_object_new (REMINDER_TYPE_UI, NULL);
	return ui;
}
