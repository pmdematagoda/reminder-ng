/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib-2.0/glib.h>
#include <gtk/gtk.h>
#include <canberra.h>
#include <glib/gi18n.h>
/* #include <libnotify/notify.h> */
#include <dbus/dbus-glib.h>
//#include <

#include "config.h"
#include "reminder-stock-icons.h"
#include "reminder-list.h"

#include "reminder-ui-alarm.h"

#define REMINDER_UI_ALARM_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), TYPE_REMINDER_UI_ALARM, ReminderUIAlarmPrivate))

G_DEFINE_TYPE (ReminderUIAlarm, reminder_ui_alarm, G_TYPE_OBJECT)

#define REMINDER_NOTIFY_TIMEOUT 30 /* s */

struct ReminderUIAlarmPrivate
{
	ca_context *alert_sound_context;

	gboolean notification_visible;
	GdkPixbuf *notify_icon;

	GtkWidget *window;
	gboolean reminder_noticed;

	DBusGConnection *dbus_connection;
	DBusGProxy *notify_connection;
};

enum
{
	REMINDER_NOTICED_SIGNAL,
	REMINDER_MISSED_SIGNAL,
	LAST_SIGNAL
};

static guint signals [LAST_SIGNAL] = { 0 };

static void reminder_ui_alarm_stop_sound (ReminderUIAlarm *ui_alarm);

static void
reminder_ui_alarm_show_notification (ReminderUIAlarm *ui_alarm,
				     const gchar *reminder_title)
{
	/* GError *error = NULL; */

	/* notify_notification_update (ui_alarm->priv->reminder_notifications, */
	/* 			    reminder_title, */
	/* 			    _("Reminder expired"), NULL); */

	/* notify_notification_set_timeout (ui_alarm->priv->reminder_notifications, */
	/* 				 REMINDER_NOTIFY_TIMEOUT * 1000); */

	/* ui_alarm->priv->notification_visible = notify_notification_show (ui_alarm->priv->reminder_notifications, */
	/* 								 &error); */

	/* if (error) { */
	/* 	g_warning (_("Unable to display notification: %s"), */
	/* 		   error->message); */
	/* 	g_error_free (error); */
	/* 	error = NULL; */
	/* } */
}

static void
reminder_ui_alarm_popup_ok_pressed_cb (GtkWidget *button,
				       ReminderUIAlarm *ui_alarm)
{
	gtk_widget_hide (ui_alarm->priv->window);
	gtk_widget_destroy (ui_alarm->priv->window);
	ui_alarm->priv->window = NULL;
	reminder_ui_alarm_stop_sound (ui_alarm);

	g_signal_emit (ui_alarm,
		       signals [REMINDER_NOTICED_SIGNAL],
		       0);
}

static void
reminder_ui_alarm_show_popup (ReminderUIAlarm *ui_alarm,
			      const gchar *reminder_title)
{
	GtkWidget *alarm_title_label,
		*ok_button,
		*main_vbox;
	GtkWidget *main_alignment;
	gchar *alarm_title;

	alarm_title = g_markup_printf_escaped (_("<b>Reminder \"%s\" has expired</b>"),
					       reminder_title);

	ui_alarm->priv->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (ui_alarm->priv->window),
			      reminder_title);
	gtk_window_set_decorated (GTK_WINDOW (ui_alarm->priv->window),
				  FALSE);
	gtk_window_set_keep_above (GTK_WINDOW (ui_alarm->priv->window),
				   TRUE);
	gtk_window_set_urgency_hint (GTK_WINDOW (ui_alarm->priv->window),
				     TRUE);
	gtk_window_set_position (GTK_WINDOW (ui_alarm->priv->window),
				 GTK_WIN_POS_CENTER_ALWAYS);
	gtk_window_set_icon_name (GTK_WINDOW (ui_alarm->priv->window),
				  REMINDER_NG_APP_ICON);

	main_alignment = gtk_alignment_new (0.5, 0.5,
					    0, 0);
	gtk_alignment_set_padding (GTK_ALIGNMENT (main_alignment),
				   12, 12,
				   12, 12);
	alarm_title_label = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (alarm_title_label),
			      alarm_title);
	g_free (alarm_title);

	ok_button = gtk_button_new_from_stock (GTK_STOCK_OK);

	main_vbox = gtk_vbox_new (FALSE, 6);

	gtk_box_pack_start (GTK_BOX (main_vbox),
			    alarm_title_label,
			    FALSE,
			    FALSE,
			    6);
	gtk_box_pack_start (GTK_BOX (main_vbox),
			    ok_button,
			    FALSE,
			    FALSE,
			    0);
	gtk_container_add (GTK_CONTAINER (main_alignment),
			   main_vbox);

	gtk_container_add (GTK_CONTAINER (ui_alarm->priv->window),
			   main_alignment);
	gtk_window_set_modal (GTK_WINDOW (ui_alarm->priv->window),
			      TRUE);

	g_signal_connect (ok_button,
			  "clicked",
			  G_CALLBACK (reminder_ui_alarm_popup_ok_pressed_cb),
			  ui_alarm);

	gtk_widget_show_all (ui_alarm->priv->window);
	gtk_main ();
}

static void
reminder_ui_alarm_play_default_sound (ReminderUIAlarm *ui_alarm)
{
	ca_context_play (ui_alarm->priv->alert_sound_context,
			 0,
			 CA_PROP_EVENT_ID, "alarm-clock-elapsed",
			 CA_PROP_EVENT_DESCRIPTION, _("Reminder Expired"), NULL);
}

static void
reminder_ui_alarm_notify_low_prio (ReminderUIAlarm *ui_alarm,
				   const gchar *reminder_title)
{
	reminder_ui_alarm_show_notification (ui_alarm,
					     reminder_title);
}

static void
reminder_ui_alarm_notify_norm_prio (ReminderUIAlarm *ui_alarm,
				    const gchar *reminder_title)
{
	reminder_ui_alarm_show_notification (ui_alarm,
					     reminder_title);
	reminder_ui_alarm_play_default_sound (ui_alarm);
}

static void
reminder_ui_alarm_notify_high_prio (ReminderUIAlarm *ui_alarm,
				    const gchar *reminder_title)
{
	reminder_ui_alarm_play_default_sound (ui_alarm);
	reminder_ui_alarm_show_popup (ui_alarm,
				      reminder_title);
}

static void
reminder_ui_alarm_stop_sound (ReminderUIAlarm *ui_alarm)
{

#ifdef CANBERRA_IS_015
#warning Disabling ability to stop sound alarms while they are playing as the version of libcanberra present does not support it
	gboolean playing = FALSE;

#else
	gboolean playing;

	ca_context_playing (ui_alarm->priv->alert_sound_context,
			    0,
			    &playing);
#endif

	if (playing == TRUE)
		ca_context_cancel (ui_alarm->priv->alert_sound_context,
				   0);
}

static void
reminder_ui_alarm_cleanup_alarms (ReminderUIAlarm *ui_alarm)
{
	GError *error = NULL;

	reminder_ui_alarm_stop_sound (ui_alarm);

	if (ui_alarm->priv->notification_visible) {
		/* notify_notification_close (ui_alarm->priv->reminder_notifications, */
		/* 			   &error); */
		if (error) {
			g_warning (_("Unable to close notification: %s"),
				   error->message);
			g_error_free (error);
		}

		/* Even in the event of an error, consider the notification
		 * as hidden
		 */
		ui_alarm->priv->notification_visible = FALSE;

		g_signal_emit (ui_alarm,
			       signals [REMINDER_MISSED_SIGNAL],
			       0);
	}
	else if (ui_alarm->priv->window) {
		gtk_widget_hide (ui_alarm->priv->window);
		gtk_main_quit ();
		gtk_widget_destroy (ui_alarm->priv->window);
		ui_alarm->priv->window = NULL;
		g_signal_emit (ui_alarm,
			       signals [REMINDER_MISSED_SIGNAL],
			       0);
	}
}

static void
reminder_ui_alarm_exec_script (const gchar *script_path)
{
	GError *error = NULL;

	if (!g_spawn_command_line_async (script_path,
					 &error))
		g_warning (_("Error: %s\n"
			     "The user specified script could not be executed"),
			   error->message);

}

void
reminder_ui_alarm_notify (ReminderUIAlarm *ui_alarm,
			  const gchar *reminder_title,
			  guint alarm_prio,
			  ReminderPrioDet *prio_det)
{
	ui_alarm->priv->reminder_noticed = FALSE;
	reminder_ui_alarm_cleanup_alarms (ui_alarm);

	switch (alarm_prio) {

		/* Low */
	case 0:
		reminder_ui_alarm_notify_low_prio (ui_alarm,
						   reminder_title);
		break;

		/* Normal */
	case 1:
		reminder_ui_alarm_notify_norm_prio (ui_alarm,
						    reminder_title);
		break;

		/* High */
	case 2:
		reminder_ui_alarm_notify_high_prio (ui_alarm,
						    reminder_title);
		break;

		/* Custom */
	case 6:
		if (prio_det->snd_alrm_play)
			reminder_ui_alarm_play_default_sound (ui_alarm);

		if (prio_det->script_path)
			reminder_ui_alarm_exec_script (prio_det->script_path);

		switch (prio_det->vis_alrm_type) {

		case 1:
			reminder_ui_alarm_show_notification (ui_alarm,
							     reminder_title);
			break;

		case 2:
			reminder_ui_alarm_show_popup (ui_alarm,
						      reminder_title);
			break;

			/* If no visual alarm has been specified, send the
			 * reminder-missed signal
			 */
		case 0:
			g_signal_emit (ui_alarm,
				       signals [REMINDER_MISSED_SIGNAL],
				       0);
			break;

			/* If an invalid visual alarm type has been specified
			 * send the reminder-missed signal and a g_warning as
			 * well
			 */
		default:
			g_warning ("Invalid vis_alrm_type specified in the call"
				   "to start an alarm");
			g_signal_emit (ui_alarm,
				       signals [REMINDER_MISSED_SIGNAL],
				       0);
			return;
		}
	}
}

/* static void */
/* reminder_ui_alarm_notify_ok_pressed_cb (NotifyNotification *notification, */
/* 					gchar *action_id, */
/* 					gpointer user_data) */
/* { */
/* 	ReminderUIAlarm *ui_alarm = (ReminderUIAlarm *) user_data; */

/* 	ui_alarm->priv->reminder_noticed = TRUE; */

/* 	reminder_ui_alarm_stop_sound (ui_alarm); */
/* } */

/* static void */
/* reminder_ui_alarm_notify_closed_cb (NotifyNotification *notification, */
/* 				    ReminderUIAlarm *ui_alarm) */
/* { */
/* 	if (ui_alarm->priv->reminder_noticed == TRUE) { */
/* 		ui_alarm->priv->reminder_noticed = FALSE; */
/* 		g_signal_emit (ui_alarm, */
/* 			       signals [REMINDER_NOTICED_SIGNAL], */
/* 			       0); */

/* 	} else { */
/* 		g_signal_emit (ui_alarm, */
/* 			       signals [REMINDER_MISSED_SIGNAL], */
/* 			       0); */
/* 	} */
/* 	ui_alarm->priv->notification_visible = FALSE; */
/* } */

void
reminder_ui_alarm_prep_finalize (ReminderUIAlarm *ui_alarm)
{
	if (ui_alarm->priv->window) {
		gtk_widget_hide (ui_alarm->priv->window);
		gtk_main_quit ();
		gtk_widget_destroy (ui_alarm->priv->window);
		ui_alarm->priv->window = NULL;
	}
}

static void
reminder_ui_alarm_finalize (GObject *object)
{
	ReminderUIAlarm *ui_alarm = REMINDER_UI_ALARM (object);
	GError *error = NULL;

	/* First cleanup any notifications being shown */
	reminder_ui_alarm_stop_sound (ui_alarm);

	/* if (ui_alarm->priv->notification_visible) { */
	/* 	notify_notification_close (ui_alarm->priv->reminder_notifications, */
	/* 				   &error); */
	/* 	if (error) { */
	/* 		g_warning (_("Unable to close notification: %s"), */
	/* 			   error->message); */
	/* 		g_error_free (error); */
	/* 	} */

	/* 	/\* Even in the event of an error, consider the notification */
	/* 	 * as hidden */
	/* 	 *\/ */
	/* 	ui_alarm->priv->notification_visible = FALSE; */
	/* } */

	/* g_object_unref (ui_alarm->priv->reminder_notifications); */
	ca_context_destroy (ui_alarm->priv->alert_sound_context);
	if (ui_alarm->priv->notify_icon)
		g_object_unref (ui_alarm->priv->notify_icon);
}

static void
reminder_ui_alarm_class_init (ReminderUIAlarmClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = reminder_ui_alarm_finalize;

	signals[REMINDER_NOTICED_SIGNAL] =
		g_signal_new ("reminder-noticed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ReminderUIAlarmClass,
					       reminder_noticed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

	signals[REMINDER_MISSED_SIGNAL] =
		g_signal_new ("reminder-missed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ReminderUIAlarmClass,
					       reminder_missed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

	g_type_class_add_private (klass, sizeof (ReminderUIAlarmPrivate));
}

static void
reminder_ui_alarm_init (ReminderUIAlarm *ui_alarm)
{
	GError *error;

	ui_alarm->priv = REMINDER_UI_ALARM_GET_PRIVATE (ui_alarm);

	ui_alarm->priv->window = NULL;
	ui_alarm->priv->notification_visible = FALSE;

	ca_context_create (&(ui_alarm->priv->alert_sound_context));
	ca_context_change_props (ui_alarm->priv->alert_sound_context,
				 CA_PROP_APPLICATION_NAME, "Reminder-ng",
				 CA_PROP_APPLICATION_ID, "org.pmd.ReminderNg",
				 CA_PROP_MEDIA_ROLE, "event", NULL);
	ca_context_open (ui_alarm->priv->alert_sound_context);


	ui_alarm->priv->dbus_connection = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
	ui_alarm->priv->notify_connection = dbus_g_proxy_new_for_name (ui_alarm->priv->dbus_connection,
								       "org.freedesktop.Notifications",
								       "/org/freedesktop/Notifications",
								       "org.freedesktop.Notifications");

	/* ui_alarm->priv->notify_icon = gtk_icon_theme_load_icon (gtk_icon_theme_get_default (), */
	/* 						  REMINDER_NG_APP_ICON, */
	/* 						  48, */
	/* 						  GTK_ICON_LOOKUP_NO_SVG, */
	/* 						  NULL); */

	/* ui_alarm->priv->reminder_notifications = notify_notification_new ("Reminder Expired", */
	/* 								  NULL, */
	/* 								  NULL, */
	/* 								  NULL); */
	/* notify_notification_add_action (ui_alarm->priv->reminder_notifications, */
	/* 				"missed-reminder-noticed", */
	/* 				_("Ok"), */
	/* 				reminder_ui_alarm_notify_ok_pressed_cb, */
	/* 				ui_alarm, */
	/* 				NULL); */
	/* if (ui_alarm->priv->notify_icon) */
	/* 	notify_notification_set_icon_from_pixbuf (ui_alarm->priv->reminder_notifications, */
	/* 						  ui_alarm->priv->notify_icon); */

	/* g_signal_connect (ui_alarm->priv->reminder_notifications, */
	/* 		  "closed", */
	/* 		  G_CALLBACK (reminder_ui_alarm_notify_closed_cb), */
	/* 		  ui_alarm); */


}

ReminderUIAlarm *
reminder_ui_alarm_new (void)
{
	ReminderUIAlarm *ui_alarm = (ReminderUIAlarm *) g_object_new (TYPE_REMINDER_UI_ALARM,
								      NULL);

	return ui_alarm;
}
