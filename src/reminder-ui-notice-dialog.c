/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtk.h>
#include <glib/gi18n.h>

#include "reminder-stock-icons.h"

#include "reminder-ui-notice-dialog.h"

static void
reminder_ui_notice_dialog_get_err (ReminderUIErrorDialog err_type,
				   gchar **error_heading_str,
				   gchar **error_desc_str);

static void
reminder_ui_notice_dialog_get_confirm (ReminderUIConfirmDialog confirm_type,
				       gchar **confirm_heading_str,
				       gchar **confirm_desc_str);

void
reminder_ui_notice_dialog_show_error (GtkWindow *parent, ReminderUIErrorDialog err_type)
{
	GtkWidget *error_dialog;

	gchar *error_heading_str, *error_desc_str;

	reminder_ui_notice_dialog_get_err (err_type, &error_heading_str,
					   &error_desc_str);

	error_dialog = gtk_message_dialog_new_with_markup (parent,
							   GTK_DIALOG_DESTROY_WITH_PARENT,
							   GTK_MESSAGE_ERROR,
							   GTK_BUTTONS_OK,
							   NULL);
	gtk_window_set_icon_name (GTK_WINDOW (error_dialog),
				  REMINDER_NG_APP_ICON);

	gtk_message_dialog_set_markup (GTK_MESSAGE_DIALOG (error_dialog),
				       error_heading_str);

	gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (error_dialog),
						  "%s",
						  error_desc_str);

	g_free (error_heading_str);
	g_free (error_desc_str);

	gtk_dialog_run (GTK_DIALOG (error_dialog));

	gtk_widget_hide (error_dialog);
	gtk_widget_destroy (error_dialog);
}

gboolean
reminder_ui_notice_dialog_show_confirm (GtkWindow *parent, ReminderUIConfirmDialog confirm_type)
{
	GtkWidget *confirm_dialog;

	gchar *confirm_heading_str, *confirm_desc_str;

	gboolean ret = FALSE;

	reminder_ui_notice_dialog_get_confirm (confirm_type, &confirm_heading_str,
					       &confirm_desc_str);

	confirm_dialog = gtk_message_dialog_new_with_markup (parent,
							     GTK_DIALOG_DESTROY_WITH_PARENT,
							     GTK_MESSAGE_QUESTION,
							     GTK_BUTTONS_YES_NO,
							     NULL);
	gtk_window_set_icon_name (GTK_WINDOW (confirm_dialog),
				  REMINDER_NG_APP_ICON);

	gtk_message_dialog_set_markup (GTK_MESSAGE_DIALOG (confirm_dialog),
				       confirm_heading_str);

	gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (confirm_dialog),
						  "%s",
						  confirm_desc_str);

	g_free (confirm_heading_str);
	g_free (confirm_desc_str);

	if (gtk_dialog_run (GTK_DIALOG (confirm_dialog)) == GTK_RESPONSE_YES)
		{
			ret = TRUE;
		}

	gtk_widget_hide (confirm_dialog);
	gtk_widget_destroy (confirm_dialog);

	return ret;
}

static void
reminder_ui_notice_dialog_get_err (ReminderUIErrorDialog err_type,
				   gchar **error_heading_str,
				   gchar **error_desc_str)
{
	switch (err_type)
	  {

	  case NEW_REMINDER_TIME_ERR:
		  *error_heading_str =
			  g_markup_printf_escaped (_("<span weight=\"bold\""
						     "size=\"larger\">Alarm time in the past</span>"));

		  *error_desc_str =
			  g_strdup (_("The reminder being added has an alarm time "
				      "in the past. Please check the details and "
				      "try to add the reminder again"));

		  break;

	  case NEW_REMINDER_VALIDATION_ERR:
		  *error_heading_str =
			  g_markup_printf_escaped (_("<span weight=\"bold\""
						     "size=\"larger\">Reminder details invalid</span>"));

		  *error_desc_str =
			  g_strdup (_("The reminder details entered are invalid."
				      "Please re-enter them again"));

		  break;

	  case NEW_REMINDER_TITLE_ERR:
		  *error_heading_str =
			  g_markup_printf_escaped (_("<span weight=\"bold\""
						     "size=\"larger\">Reminder title blank</span>"));

		  *error_desc_str =
			  g_strdup (_("Please enter a reminder title"));

		  break;

	  case NEW_REMINDER_EPOCH_ERR:
		  *error_heading_str =
			  g_markup_printf_escaped (_("<span weight=\"bold\""
						     "size=\"larger\">Reminder with same alarm time exists</span>"));

		  *error_desc_str =
			  g_strdup (_("A reminder with the same alarm time as the one "
				      "you are currently adding is already present. "
				      "Please change the alarm time (an extra minute would be enough) "
				      "and try to add the reminder again"));

		  break;

	  default:
		  *error_heading_str = g_markup_printf_escaped (_("<span weight=\"bold\""
								  "size=\"larger\">Unknown error</span>"));
		  *error_desc_str = g_strdup (_("Unknown error"));
	  }

}

static void
reminder_ui_notice_dialog_get_confirm (ReminderUIConfirmDialog confirm_type,
				       gchar **confirm_heading_str,
				       gchar **confirm_desc_str)
{
	switch (confirm_type)
	  {

	  case DELETE_EXPIRED_REMINDERS:
		  *confirm_heading_str =
			  g_markup_printf_escaped (_("<span weight=\"bold\""
						     "size=\"larger\">Expired reminders present</span>"));

		  *confirm_desc_str =
			  g_strdup (_("You specified that expired reminders were to be deleted, "
				      "however there are some expired reminders already present "
				      "before the option to discard them was enabled. "
				      "Would you like to purge all the expired reminders?"));

		  break;

	  case PURGE_REMINDERS:
		  *confirm_heading_str =
			  g_markup_printf_escaped (_("<span weight=\"bold\""
						     "size=\"larger\">Really purge reminders?</span>"));

		  *confirm_desc_str =
			  g_strdup (_("Do you really want to purge the reminders?\n"));

	  default:
		  *confirm_heading_str = g_markup_printf_escaped (_("<span weight=\"bold\""
								    "size=\"larger\">Unknown question</span>"));
		  *confirm_desc_str = g_strdup (_("Unknown question"));
	  }

}
