/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib-object.h>

G_BEGIN_DECLS
#define REMINDER_TYPE_UI_PREFERENCES           (reminder_ui_preferences_get_type ())
#define REMINDER_UI_PREFERENCES(o)             (G_TYPE_CHECK_INSTANCE_CAST ((o), REMINDER_TYPE_UI_PREFERENCES, ReminderUIPreferences))
#define REMINDER_UI_PREFERENCES_CLASS(k)       (G_TYPE_CHECK_CLASS_CAST ((k), REMINDER_TYPE_UI_PREFERENCES, ReminderUIPreferencesClass))
#define REMINDER_IS_UI_PREFERENCES(o)          (G_TYPE_CHECK_INSTANCE_TYPE ((o), REMINDER_TYPE_UI_PREFERENCES))
#define REMINDER_IS_UI_PREFERENCES_CLASS(k)    (G_TYPE_CHECK_CLASS_TYPE ((k), REMINDER_TYPE_UI_PREFERENCES))
#define REMINDER_UI_PREFERENCES_GET_CLASS(o)   (G_TYPE_INSTANCE_GET_CLASS ((k), REMINDER_TYPE_UI_PREFERENCES, ReminderUIPreferences))
typedef struct ReminderUIPreferencesPrivate ReminderUIPreferencesPrivate;

typedef struct
{
	GObject                              parent;
	ReminderUIPreferencesPrivate        *priv;
} ReminderUIPreferences;

typedef struct
{
	GObjectClass     parent_class;

} ReminderUIPreferencesClass;

GType                    reminder_ui_preferences_get_type                  (void);
ReminderUIPreferences   *reminder_ui_preferences_new                       (void);
void                     reminder_ui_preferences_show                      (ReminderUIPreferences *ui_preferences);
void                     reminder_ui_preferences_prep_finalize             (ReminderUIPreferences *preferences);

G_END_DECLS
