/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib-object.h>

G_BEGIN_DECLS
#define REMINDER_TYPE_CORE           (reminder_core_get_type ())
#define REMINDER_CORE(o)             (G_TYPE_CHECK_INSTANCE_CAST ((o), REMINDER_TYPE_CORE, ReminderCore))
#define REMINDER_CORE_CLASS(k)       (G_TYPE_CHECK_CLASS_CAST ((k), REMINDER_TYPE_CORE, ReminderCoreClass))
#define REMINDER_IS_CORE(o)          (G_TYPE_CHECK_INSTANCE_TYPE ((o), REMINDER_TYPE_CORE))
#define REMINDER_IS_CORE_CLASS(k)    (G_TYPE_CHECK_CLASS_TYPE ((k), REMINDER_TYPE_CORE))
#define REMINDER_CORE_GET_CLASS(o)   (G_TYPE_INSTANCE_GET_CLASS ((k), REMINDER_TYPE_CORE, ReminderCore))

typedef struct ReminderCorePrivate ReminderCorePrivate;

typedef struct
{
	GObject                 parent;
	ReminderCorePrivate    *priv;
} ReminderCore;

typedef struct
{
	GObjectClass    parent_class;

	void            (*reminder_expired)     (ReminderCore *core,
						 glong reminder_epoch);
} ReminderCoreClass;

typedef enum
{
	CURRENT_TREE,
	EXPIRED_TREE
} ReminderCoreReminderTreeType;

typedef enum
{
	CURRENT_REMINDER,
	EXPIRED_REMINDER
} ReminderCoreReminderType;

GType                   reminder_core_get_type               (void);
ReminderCore           *reminder_core_new                    (void);
void                    reminder_core_build_tree             (ReminderCore *core,
							      GtkTreeView *tree_view,
							      ReminderCoreReminderTreeType tree_type);
void                    reminder_core_add_reminder           (ReminderCore *core,
							      ReminderList *reminder_list,
							      ReminderCoreReminderType reminder_type);
void                    reminder_core_delete_reminder        (ReminderCore *core,
							      long int epoch,
							      ReminderCoreReminderType reminder_type);
ReminderList           *reminder_core_get_reminder           (ReminderCore *core,
							      long int epoch);
void                    reminder_core_purge_reminders        (ReminderCore *core,
							      ReminderCoreReminderType reminder_type);
void                    reminder_core_get_missed_reminders   (ReminderCore *core,
							      ReminderList **missed_list);
void                    reminder_core_set_reminder_noticed   (ReminderCore *core,
							      glong reminder_epoch);
G_END_DECLS
