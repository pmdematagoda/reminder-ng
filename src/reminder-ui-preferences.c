/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include <glib/gi18n.h>

#include "reminder-common.h"
#include "reminder-stock-icons.h"

#include "reminder-ui-preferences.h"

#define REMINDER_UI_PREFERENCES_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), REMINDER_TYPE_UI_PREFERENCES, ReminderUIPreferencesPrivate))

G_DEFINE_TYPE (ReminderUIPreferences, reminder_ui_preferences, G_TYPE_OBJECT)
     struct ReminderUIPreferencesPrivate
     {
	     GConfClient *gconf_client;

	     GtkWidget *dialog,
		     *pref_notebook,
		     *pref_notebook_alignment,
		     *general_vbox,
		     *general_internal_vbox,
		     *discard_on_expire_check_button;

     };

void reminder_ui_preferences_show (ReminderUIPreferences *ui_preferences)
{
	if (gconf_client_get_bool (ui_preferences->priv->gconf_client,
				   REMINDER_EXPIRED_DISCARD, NULL) == FALSE)
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
					      (ui_preferences->priv->discard_on_expire_check_button),
					      FALSE);
	else
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
					      (ui_preferences->priv->discard_on_expire_check_button),
					      TRUE);

	gtk_widget_show_all (ui_preferences->priv->dialog);
	gtk_dialog_run (GTK_DIALOG (ui_preferences->priv->dialog));

	gtk_widget_hide (ui_preferences->priv->dialog);
}

static void
reminder_ui_preferences_discard_on_expire_cb (GtkWidget *check_button,
					      ReminderUIPreferences *ui_preferences)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (check_button)))
		gconf_client_set_bool (ui_preferences->priv->gconf_client,
				       REMINDER_EXPIRED_DISCARD, TRUE, NULL);
	else
		gconf_client_set_bool (ui_preferences->priv->gconf_client,
				       REMINDER_EXPIRED_DISCARD, FALSE, NULL);
}

void
reminder_ui_preferences_prep_finalize (ReminderUIPreferences *ui_preferences)
{
	if (gtk_widget_get_visible (ui_preferences->priv->dialog)) {
		gtk_dialog_response (GTK_DIALOG (ui_preferences->priv->dialog),
				     GTK_RESPONSE_DELETE_EVENT);
	}
}

static void
reminder_ui_preferences_finalize (GObject *object)
{
	ReminderUIPreferences *ui_preferences =
		REMINDER_UI_PREFERENCES (object);

	g_object_unref (ui_preferences->priv->gconf_client);

	gtk_widget_destroy (ui_preferences->priv->dialog);
}

static void
reminder_ui_preferences_class_init (ReminderUIPreferencesClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = reminder_ui_preferences_finalize;

	g_type_class_add_private (klass,
				  sizeof (ReminderUIPreferencesPrivate));
}

static void
reminder_ui_preferences_init (ReminderUIPreferences *ui_preferences)
{
	ui_preferences->priv =
		REMINDER_UI_PREFERENCES_GET_PRIVATE (ui_preferences);

	ui_preferences->priv->gconf_client = gconf_client_get_default ();
	ui_preferences->priv->dialog =
		gtk_dialog_new_with_buttons (_("Reminder-ng Preferences"),
					     NULL,
					     GTK_DIALOG_MODAL,
					     GTK_STOCK_CLOSE,
					     GTK_RESPONSE_CLOSE, NULL);
	/* gtk_dialog_set_has_separator (GTK_DIALOG (ui_preferences->priv->dialog), */
	/* 			      FALSE); */
	gtk_window_set_icon_name (GTK_WINDOW (ui_preferences->priv->dialog),
				  REMINDER_NG_APP_ICON);

	ui_preferences->priv->pref_notebook = gtk_notebook_new ();
	ui_preferences->priv->pref_notebook_alignment = gtk_alignment_new (0.5, 0.5,
									   1, 1);
	gtk_alignment_set_padding (GTK_ALIGNMENT (ui_preferences->priv->pref_notebook_alignment),
				   0, 0,
				   6, 6);

	gtk_container_add (GTK_CONTAINER (ui_preferences->priv->pref_notebook_alignment), ui_preferences->priv->pref_notebook);

	ui_preferences->priv->general_vbox = gtk_vbox_new (FALSE, 0);
	ui_preferences->priv->general_internal_vbox = gtk_vbox_new (FALSE, 0);

	ui_preferences->priv->discard_on_expire_check_button =
		gtk_check_button_new_with_label (_("Delete reminders after they "
						   "expire"));

	g_signal_connect (ui_preferences->priv->discard_on_expire_check_button,
			  "toggled",
			  G_CALLBACK (reminder_ui_preferences_discard_on_expire_cb),
			  ui_preferences);

	gtk_box_pack_start (GTK_BOX (ui_preferences->priv->general_internal_vbox),
			    ui_preferences->priv->discard_on_expire_check_button,
			    FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (ui_preferences->priv->general_vbox),
			    ui_preferences->priv->general_internal_vbox,
			    FALSE, FALSE, 12);

	gtk_notebook_insert_page (GTK_NOTEBOOK
				  (ui_preferences->priv->pref_notebook),
				  ui_preferences->priv->general_vbox,
				  gtk_label_new (_("General")), -1);

	gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (ui_preferences->priv->dialog))),
			    ui_preferences->priv->pref_notebook_alignment, TRUE, TRUE,
			    12);
	gtk_window_set_resizable (GTK_WINDOW (ui_preferences->priv->dialog),
				  FALSE);
}

ReminderUIPreferences *
reminder_ui_preferences_new ()
{
	ReminderUIPreferences *ui_preferences;

	ui_preferences = g_object_new (REMINDER_TYPE_UI_PREFERENCES, NULL);

	return ui_preferences;
}
