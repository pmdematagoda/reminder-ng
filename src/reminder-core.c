/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>

#include <glib-2.0/glib.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <gconf/gconf-client.h>
#include <glib-object.h>

#include "reminder-list.h"
#include "reminder-file-ops.h"
#include "reminder-core.h"
#include "reminder-ui.h"
#include "reminder-common.h"

#define REMINDER_CORE_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), REMINDER_TYPE_CORE, ReminderCorePrivate))

G_DEFINE_TYPE (ReminderCore, reminder_core, G_TYPE_OBJECT)

struct ReminderCorePrivate
{
	FILE *database;

	GConfClient *gconf_client;

	guint curr_timeout_id;

	ReminderList *current_list;
	ReminderList *expired_list;

};

enum
{
	REMINDER_EXPIRED_SIGNAL,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static gboolean reminder_core_alarm (gpointer data);
static void 	reminder_core_write_reminders_to_database (ReminderCore *core);
static void     reminder_core_transfer_timeout_to_current (ReminderCore *core);

void
reminder_core_get_missed_reminders (ReminderCore *core,
				    ReminderList **missed_list)
{
	ReminderList *rem_list;

	for (rem_list = core->priv->expired_list;
	     rem_list != NULL;
	     rem_list = rem_list->next)
		{
			/* If the noticed flag hasn't been set on the reminder,
			 * add it to the missed list
			 */
			if (!reminder_list_misc_param_exist (rem_list,
							     RC_NOTICED_PARAM))
				/* Don't worry about the priority here */
				reminder_list_add_node (missed_list,
							rem_list->reminder_prio,
							rem_list->reminder_title,
							rem_list->reminder_hour,
							rem_list->reminder_minute,
							rem_list->reminder_second,
							rem_list->reminder_day,
							rem_list->reminder_month,
							rem_list->reminder_year,
							rem_list->reminder_tz,
							rem_list->reminder_epoch,
							NULL,
							DESCEND_SORT);
		}
}

void
reminder_core_set_reminder_noticed (ReminderCore *core, glong reminder_epoch)
{
	ReminderList *rem_list;

	/* If all the reminders are to be marked and if the expired reminders are to be deleted,
	 * then just delete all the reminders
	 */
	if (reminder_epoch == -1 && gconf_client_get_bool (core->priv->gconf_client,
							   REMINDER_EXPIRED_DISCARD, NULL) == TRUE)
		{
			reminder_list_delete_node (&(core->priv->expired_list),
						   DELETE_ALL,
						   0);
		} else {

		for (rem_list = core->priv->expired_list;
		     rem_list != NULL;
		     rem_list = rem_list->next)
			{

				if (reminder_epoch == -1 && !reminder_list_misc_param_exist (rem_list, RC_NOTICED_PARAM))
					reminder_list_misc_param_add (rem_list,
								      RC_NOTICED_PARAM,
								      NULL);
				else {

					if (reminder_epoch == rem_list->reminder_epoch) {
						
						if (gconf_client_get_bool (core->priv->gconf_client,
									   REMINDER_EXPIRED_DISCARD,
									   NULL) == TRUE)
							reminder_list_delete_node (&(core->priv->expired_list),
										   NORMAL_DELETE,
										   reminder_epoch);
						else if (!reminder_list_misc_param_exist  (rem_list, RC_NOTICED_PARAM))
							 reminder_list_misc_param_add (rem_list,
										       RC_NOTICED_PARAM,
										       NULL);

						break;
					}
				}
			}
	}	
	reminder_core_write_reminders_to_database (core);
}

void
reminder_core_delete_reminder (ReminderCore *core, long int epoch,
			       ReminderCoreReminderType reminder_type)
{
	long int old_epoch = 0;

	g_return_if_fail (core != NULL);
	g_return_if_fail (REMINDER_IS_CORE (core));

	switch (reminder_type)
	  {

	  case CURRENT_REMINDER:
		  g_return_if_fail (core->priv->current_list != NULL);

		  old_epoch = core->priv->current_list->reminder_epoch;

		  reminder_list_delete_node (&(core->priv->current_list),
					     NORMAL_DELETE,
					     epoch);

		  if (core->priv->current_list)
			  if (core->priv->current_list->reminder_epoch != old_epoch)
				  reminder_core_transfer_timeout_to_current (core);

		  break;

	  case EXPIRED_REMINDER:
		  g_return_if_fail (core->priv->expired_list != NULL);

		  reminder_list_delete_node (&(core->priv->expired_list),
					     NORMAL_DELETE,
					     epoch);
		  break;

	  default:
		  g_warning ("Invalid reminder_type specified. This may be due to a bug");
	  }
	reminder_core_write_reminders_to_database (core);
}

void
reminder_core_add_reminder (ReminderCore *core, ReminderList *new_reminder,
			    ReminderCoreReminderType reminder_type)
{
	long int old_epoch = 0;
	ReminderList *added_node;

	g_return_if_fail (core != NULL);
	g_return_if_fail (REMINDER_IS_CORE (core));
	g_return_if_fail (new_reminder != NULL);
	switch (reminder_type)
	  {

	  case CURRENT_REMINDER:
		  g_return_if_fail (new_reminder->reminder_epoch > time (NULL));

		  if (core->priv->current_list)
			  old_epoch = core->priv->current_list->reminder_epoch;

		  added_node = reminder_list_add_node (&(core->priv->current_list),
						       new_reminder->reminder_prio,
						       new_reminder->reminder_title,
						       new_reminder->reminder_hour,
						       new_reminder->reminder_minute,
						       new_reminder->reminder_second,
						       new_reminder->reminder_day,
						       new_reminder->reminder_month,
						       new_reminder->reminder_year,
						       new_reminder->reminder_tz,
						       new_reminder->reminder_epoch,
						       NULL,
						       ASCEND_SORT);

		  if (new_reminder->reminder_prio == 6)
			  reminder_list_set_prio_det (added_node,
						      new_reminder->prio_det->vis_alrm_type,
						      new_reminder->prio_det->snd_alrm_play,
						      new_reminder->prio_det->script_path);

		  if (old_epoch != core->priv->current_list->reminder_epoch)
			  reminder_core_transfer_timeout_to_current (core);
		  break;

	  case EXPIRED_REMINDER:
		  added_node = reminder_list_add_node (&(core->priv->expired_list),
						       new_reminder->reminder_prio,
						       new_reminder->reminder_title,
						       new_reminder->reminder_hour,
						       new_reminder->reminder_minute,
						       new_reminder->reminder_second,
						       new_reminder->reminder_day,
						       new_reminder->reminder_month,
						       new_reminder->reminder_year,
						       new_reminder->reminder_tz,
						       new_reminder->reminder_epoch,
						       NULL,
						       ASCEND_SORT);

		  if (new_reminder->reminder_prio == 6)
			  reminder_list_set_prio_det (added_node,
						      new_reminder->prio_det->vis_alrm_type,
						      new_reminder->prio_det->snd_alrm_play,
						      new_reminder->prio_det->script_path);

		  break;

	  default:
		  g_warning ("Wrong reminder_type specified. This may be due to a bug.\n");
	  }
	reminder_core_write_reminders_to_database (core);
}

void
reminder_core_purge_reminders (ReminderCore *core,
			       ReminderCoreReminderType reminder_type)
{
	switch (reminder_type) {

	case CURRENT_REMINDER:
		reminder_list_delete_node (&(core->priv->current_list),
					   DELETE_ALL, 0);
		break;

	case EXPIRED_REMINDER:
		reminder_list_delete_node (&(core->priv->expired_list),
					   DELETE_ALL, 0);
		break;

	default:
		g_warning ("Incorrect reminder type specified");
	}
	reminder_core_write_reminders_to_database (core);
}

ReminderList *
reminder_core_get_reminder (ReminderCore *core, long int epoch)
{
	ReminderList *reminder_list,
		*ret = NULL;

	if (epoch > time (NULL))
		reminder_list = core->priv->current_list;
	else
		reminder_list = core->priv->expired_list;

	while (reminder_list != NULL)
	  {
		  if (reminder_list->reminder_epoch == epoch) {
			  ret = (ReminderList *) g_memdup ((gconstpointer) reminder_list,
							   sizeof (ReminderList));

			  ret->reminder_title = g_strdup (reminder_list->reminder_title);

			  if (ret->reminder_prio == 6) {
				  ret->prio_det = (ReminderPrioDet *) g_memdup ((gconstpointer) reminder_list->prio_det,
										sizeof (ReminderPrioDet));

				  if (reminder_list->prio_det->script_path)
					  ret->prio_det->script_path = g_strdup (reminder_list->prio_det->script_path);
			  }

			  ret->reminder_misc = NULL;
			  ret->next = NULL;
			  break;
		  }
		  else
			  reminder_list = reminder_list->next;
	  }

	return ret;
}

/* This is the only function where the core actually modifies variables in the
 * ReminderUI object. This should be called only once to initialise the tree view
 * widgets after which the UI and the Core maintains the reminders and widgets separately
 * in sync
 */
void
reminder_core_build_tree (ReminderCore *core, GtkTreeView *tree_view,
			  ReminderCoreReminderTreeType tree_type)
{
	GtkCellRenderer *renderer,
		*icon_renderer;
	GtkTreeIter iter;
	GtkListStore *list_store;
	GtkTreeViewColumn *column;
	ReminderList *reminder_list = NULL;

	g_return_if_fail (tree_view != NULL);
	g_return_if_fail (GTK_IS_TREE_VIEW (tree_view));

	switch (tree_type)
	  {

	  case CURRENT_TREE:
		  reminder_list = core->priv->current_list;
		  break;

	  case EXPIRED_TREE:
		  if (gconf_client_get_bool (core->priv->gconf_client,
					     REMINDER_EXPIRED_DISCARD, NULL) == FALSE)
			  reminder_list = core->priv->expired_list;
		  else
			  /* If reminders are set to be deleted when they expire,
			   * skip any un-noticed reminders lying around in the
			   * expired list as they will be deleted anyway
			   */
			  for (reminder_list = core->priv->expired_list;
			       reminder_list != NULL;
			       reminder_list = reminder_list->next)
				  if (reminder_list_misc_param_exist (reminder_list, RC_NOTICED_PARAM))
					  break;
		  break;

	  default:
		  g_warning ("Incorrect tree_type specified");
		  return;
	  }

	list_store =
		gtk_list_store_new (NO_OF_COL,
				    G_TYPE_STRING,
				    G_TYPE_STRING,
				    G_TYPE_STRING,
				    G_TYPE_STRING,
				    G_TYPE_INT);

	while (reminder_list != NULL)
	  {
		  gchar *date_str, *time_str;

		  date_str =
			  g_strdup_printf ("%d/%d/%d",
					   reminder_list->reminder_day,
					   reminder_list->reminder_month,
					   reminder_list->reminder_year);
		  time_str =
			  g_strdup_printf ("%d:%d:%d",
					   reminder_list->reminder_hour,
					   reminder_list->reminder_minute,
					   reminder_list->reminder_second);

		  if (tree_type == CURRENT_TREE)
			  gtk_list_store_append (list_store, &iter);
		  else
			  gtk_list_store_prepend (list_store, &iter);

		  gtk_list_store_set (list_store, &iter,
				      PRIORITY_COL, reminder_common_get_priority_icon_name (reminder_list->reminder_prio),
				      TITLE_COL, reminder_list->reminder_title,
				      DATE_COL, date_str,
				      TIME_COL, time_str,
				      EPOCH_COL, reminder_list->reminder_epoch,
				      -1);

		  reminder_list = reminder_list->next;
		  g_free (date_str);
		  g_free (time_str);
	  }

	renderer = (GtkCellRenderer *) gtk_cell_renderer_text_new ();
	icon_renderer = (GtkCellRenderer *) gtk_cell_renderer_pixbuf_new ();

	gtk_tree_view_insert_column_with_attributes (tree_view,
						     -1, _("Priority"),
						     icon_renderer, "icon-name",
						     PRIORITY_COL, NULL);

	gtk_tree_view_insert_column_with_attributes (tree_view,
						     -1, _("Reminder"),
						     renderer, "text",
						     TITLE_COL, NULL);

	gtk_tree_view_insert_column_with_attributes (tree_view,
						     -1, _("Date"),
						     renderer, "text",
						     DATE_COL, NULL);

	gtk_tree_view_insert_column_with_attributes (tree_view,
						     -1, _("Time"),
						     renderer, "text",
						     TIME_COL, NULL);
	gtk_tree_view_insert_column_with_attributes (tree_view,
						     -1, _("Reminder Epoch"),
						     renderer, "text",
						     EPOCH_COL, NULL);

	column = gtk_tree_view_get_column (tree_view, EPOCH_COL);
	gtk_tree_view_column_set_visible (column, FALSE);

	column = gtk_tree_view_get_column (tree_view, TITLE_COL);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_column_set_sizing (column, GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (column, 120);

	gtk_tree_view_set_model (tree_view, GTK_TREE_MODEL (list_store));

}

static void
reminder_core_transfer_timeout_to_current (ReminderCore *core)
{
	int new_time;

	if (core->priv->curr_timeout_id > 0) {
		g_source_remove (core->priv->curr_timeout_id);
	}

	if (core->priv->current_list) {
		new_time =
			(int) (core->priv->current_list->reminder_epoch - (int) time (NULL));

		core->priv->curr_timeout_id =
			g_timeout_add_seconds (new_time,
					       reminder_core_alarm, core);
	}
	else
		core->priv->curr_timeout_id = 0;
}

static void
reminder_core_write_reminders_to_database (ReminderCore *core)
{
	rewind (core->priv->database);
	if (ftruncate (fileno (core->priv->database), 0))
		g_error (_("Error %d: %s\n"
			   "Unable to clear database file,"
			   "reminder-ng will now exit"),
			 errno,
			 strerror (errno));

	flush_reminders_file (core->priv->database,
			      core->priv->expired_list);
	flush_reminders_file (core->priv->database,
			      core->priv->current_list);
}

static gboolean
reminder_core_alarm (gpointer data)
{
	ReminderCore *core = REMINDER_CORE (data);
	glong expired_reminder_epoch;

	expired_reminder_epoch = core->priv->current_list->reminder_epoch;

	reminder_list_transfer_first_node (&(core->priv->current_list),
					   &(core->priv->expired_list));

	reminder_core_write_reminders_to_database (core);
	reminder_core_transfer_timeout_to_current (core);

	g_signal_emit (core,
		       signals[REMINDER_EXPIRED_SIGNAL],
		       0,
		       expired_reminder_epoch,
		       G_TYPE_NONE);

	return FALSE;
}

static void
reminder_core_finalize (GObject *object)
{
	ReminderCore *core;

	g_return_if_fail (object != NULL);
	g_return_if_fail (REMINDER_IS_CORE (object));

	core = REMINDER_CORE (object);

	fclose (core->priv->database);

	if (core->priv->curr_timeout_id > 0) {
		g_source_remove (core->priv->curr_timeout_id);
	}

	reminder_list_delete_node (&(core->priv->current_list), DELETE_ALL, 0);
	reminder_list_delete_node (&(core->priv->expired_list), DELETE_ALL, 0);

	g_object_unref (core->priv->gconf_client);

}

static void
reminder_core_class_init (ReminderCoreClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = reminder_core_finalize;

	signals[REMINDER_EXPIRED_SIGNAL] =
		g_signal_new ("reminder-expired",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ReminderCoreClass,
					       reminder_expired), NULL, NULL,
			      g_cclosure_marshal_VOID__LONG, G_TYPE_NONE, 1,
			      G_TYPE_LONG);

	g_type_class_add_private (klass, sizeof (ReminderCorePrivate));
}

static void
reminder_core_init (ReminderCore *core)
{
	core->priv = REMINDER_CORE_GET_PRIVATE (core);

	core->priv->current_list = NULL;
	core->priv->expired_list = NULL;

	core->priv->database = open_reminder_database ();

	reminder_load_database (core->priv->database,
			      &(core->priv->current_list),
			      &(core->priv->expired_list));

	core->priv->curr_timeout_id = 0;
	reminder_core_transfer_timeout_to_current (core);

	core->priv->gconf_client = gconf_client_get_default ();
}

ReminderCore *
reminder_core_new (void)
{
	ReminderCore *core = g_object_new (REMINDER_TYPE_CORE, NULL);
	return core;
}
