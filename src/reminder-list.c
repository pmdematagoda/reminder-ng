/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <string.h>

#include <glib-2.0/glib.h>
#include <glib/gi18n.h>
#include <glib-object.h>

#include "reminder-list.h"

/* Possible miscellanous parameters to add to
 * a reminder
 */
const gchar *reminder_list_misc_param [NUM_OF_PARAMS] =
	{ "n",
	  "ns",
	  "po"
	};

static gboolean
reminder_list_add_here (gint old_epoch,
			gint new_epoch,
			ReminderListAddSortType sort_type)
{
	gboolean ret = FALSE;

	switch (sort_type) {

	case ASCEND_SORT:
		if (new_epoch < old_epoch)
			ret = TRUE;
		break;

	case DESCEND_SORT:
		if (new_epoch > old_epoch)
			ret = TRUE;
		break;

	default:
		g_warning ("Wrong sort_type specified");
	}

	return ret;
}

ReminderList *
reminder_list_add_node (ReminderList **reminder_list, guint reminder_prio,
			gchar *reminder_title, guint reminder_hour,
			guint reminder_minute, guint reminder_second,
			guint reminder_day, guint reminder_month,
			guint reminder_year, guint reminder_tz,
			gint reminder_epoch, gchar *reminder_misc,
			ReminderListAddSortType sort_type)
{
	ReminderList *navigator_ptr, *worker_ptr, *placeholder_ptr;
	gchar **reminder_misc_elem;
	gchar **reminder_misc_val;
	guint x;

	if (*reminder_list == NULL) {
		*reminder_list = (ReminderList *) g_malloc (sizeof (ReminderList));
		worker_ptr = *reminder_list;
		worker_ptr->next = NULL;
	} else {
		navigator_ptr = *reminder_list;

		if (reminder_list_add_here (navigator_ptr->reminder_epoch,
					    reminder_epoch,
					    sort_type))
			{
				worker_ptr =
					(ReminderList *) g_malloc (sizeof (ReminderList));
				worker_ptr->next = *reminder_list;
				*reminder_list = worker_ptr;
			}
		else
			{

				/* Tread carefully, potential memory leaks galore in this area */
				while (navigator_ptr->next != NULL)
					{

						if (reminder_list_add_here (navigator_ptr->next->reminder_epoch,
									    reminder_epoch,
									    sort_type))
							{
								placeholder_ptr =
									navigator_ptr->next;
								navigator_ptr->next = (ReminderList *) g_malloc (sizeof (ReminderList));

								worker_ptr =
									navigator_ptr->next;
								worker_ptr->next =
									placeholder_ptr;
								/* Crucial statement, otherwise the process keeps malloc'ing until
								 * you run out of memory
								 */
								break;
							}
						else
							navigator_ptr =
								navigator_ptr->next;

					}
				/* End of danger zone */

				if (navigator_ptr->next == NULL) {
					navigator_ptr->next = (ReminderList *) g_malloc (sizeof (ReminderList));
					worker_ptr = navigator_ptr->next;
					worker_ptr->next = NULL;
				}
			}
	}

	worker_ptr->reminder_prio = reminder_prio;

	if (reminder_title == NULL) {
		worker_ptr->reminder_title = g_strdup (_("Unknown title"));
	}
	else if (!strlen (reminder_title)) {
		worker_ptr->reminder_title = g_strdup (_("Unknown title"));
	}
	else {
		worker_ptr->reminder_title = (gchar *) g_malloc (strlen (reminder_title) + 1);
		strncpy (worker_ptr->reminder_title,
			 reminder_title,
			 (strlen (reminder_title) + 1));
	}

	worker_ptr->reminder_hour = reminder_hour;
	worker_ptr->reminder_minute = reminder_minute;
	worker_ptr->reminder_second = reminder_second;
	worker_ptr->reminder_day = reminder_day;
	worker_ptr->reminder_month = reminder_month;
	worker_ptr->reminder_year = reminder_year;
	worker_ptr->reminder_tz = reminder_tz;
	worker_ptr->reminder_epoch = reminder_epoch;

	worker_ptr->reminder_misc = NULL;

	worker_ptr->reminder_misc_table = g_hash_table_new_full (g_str_hash,
								 g_str_equal,
								 NULL,
								 g_free);

	if (reminder_misc) {
		reminder_misc_elem = g_strsplit (reminder_misc,
						 "|",
						 -1);

		if (g_strcmp0 (reminder_misc_elem [0], "-")) {

			for (x = 0;
			     reminder_misc_elem [x] != NULL;
			     x++)
				{
					worker_ptr->reminder_misc = g_list_insert (worker_ptr->reminder_misc,
										   reminder_misc_elem [x],
										   -1);

					if (strchr (reminder_misc_elem [x], '=') != NULL) {
						reminder_misc_val = g_strsplit (reminder_misc_elem [x],
										"=",
										-1);

						g_hash_table_insert (worker_ptr->reminder_misc_table,
								     reminder_misc_val[0],
								     reminder_misc_val[1]);

					} else {
						g_hash_table_insert (worker_ptr->reminder_misc_table,
								     reminder_misc_elem [x],
								     NULL);

					}
				}
		} else
			g_free (reminder_misc_elem [0]);

		g_free (reminder_misc_elem);
	}

	if (reminder_prio == 6) {
		worker_ptr->prio_det = (ReminderPrioDet *) g_malloc (sizeof (ReminderPrioDet));

		/* Initialise the priority details to corroborate with those of the "Normal"
		 * priority in case it is forgotten to set them, which shouldn't happen
		 */
		worker_ptr->prio_det->vis_alrm_type = 1;
		worker_ptr->prio_det->snd_alrm_play = TRUE;
		worker_ptr->prio_det->script_path = NULL;
	}

	return worker_ptr;
}

void
reminder_list_delete_node (ReminderList **reminder_list,
			   ReminderListDeleteAction delete_action,
			   gint reminder_epoch)
{
	ReminderList *navigator_ptr, *hunting_ptr;

	navigator_ptr = *reminder_list;

	switch (delete_action)
	  {

	  case NORMAL_DELETE:
		  /* Do not use empty lists */
		  g_return_if_fail (navigator_ptr != NULL);

		  if (navigator_ptr->reminder_epoch == reminder_epoch) {
			  *reminder_list = navigator_ptr->next;
			  break;
		  }

		  while (navigator_ptr->next != NULL)
		    {
			    if (navigator_ptr->next->reminder_epoch ==
				reminder_epoch)
			      {
				      hunting_ptr = navigator_ptr;
				      navigator_ptr = navigator_ptr->next;
				      hunting_ptr->next = navigator_ptr->next;
				      break;
			      }
			    else
				    navigator_ptr = navigator_ptr->next;

		    }
		  break;

	  case FIRST_IN_LINE:
		  /* Do not use empty lists */
		  g_return_if_fail (navigator_ptr != NULL);

		  *reminder_list = navigator_ptr->next;
		  break;

	  case DELETE_ALL:
		  /* This call is used whenever the application is closed
		   * so don't give a warning if the list is empty
		   */
		  while (navigator_ptr != NULL) {
			  hunting_ptr = navigator_ptr;
			  navigator_ptr = navigator_ptr->next;

			  reminder_list_free_node (hunting_ptr);
		  }
		  *reminder_list = NULL;

		  break;
	  }

	if (navigator_ptr) {
		reminder_list_free_node (navigator_ptr);
	}
}

void
reminder_list_transfer_first_node (ReminderList **source_list,
				   ReminderList **destination_list)
{
	ReminderList *placeholder_ptr;

	g_return_if_fail (*source_list != NULL);

	placeholder_ptr = (*source_list)->next;

	(*source_list)->next = *destination_list;
	*destination_list = *source_list;

	*source_list = placeholder_ptr;

}

void
reminder_list_free_node (ReminderList *target_node)
{
	g_return_if_fail (target_node != NULL);

	if (target_node->reminder_prio == 6) {
		if (target_node->prio_det->script_path)
			g_free (target_node->prio_det->script_path);

		g_free (target_node->prio_det);
	}

	g_list_foreach (target_node->reminder_misc,
			(GFunc) g_free,
			NULL);
	g_list_free (target_node->reminder_misc);

	g_hash_table_destroy (target_node->reminder_misc_table);

	g_free (target_node->reminder_title);
	g_free (target_node);
}


static gpointer
reminder_list_clone_data (gpointer data,
			  ReminderListMiscParams param_type) {
	gpointer ret = NULL;
	gint param_val_type = reminder_list_misc_params_type [param_type];

	if (param_val_type == G_TYPE_NONE)
		return ret;

	else if (param_val_type == G_TYPE_INT)
		ret = g_memdup (data, sizeof (gint));

	else if (param_val_type == G_TYPE_STRING)
		ret = g_memdup (data, strlen (data) + 1);

	else if (param_val_type == G_TYPE_BOOLEAN)
		ret = g_memdup (data, sizeof (gboolean));

	return ret;

}

void
reminder_list_misc_param_add (ReminderList *target_node,
			      ReminderListMiscParams param_type,
			      gpointer data)
{
	const gchar *misc_param;

	g_return_if_fail (target_node != NULL);

	if (param_type > NUM_OF_PARAMS) {
		g_warning ("Invalid parameter type specified to be added");
		return;
	}

	misc_param = reminder_list_misc_param [param_type];

	if (!g_hash_table_lookup_extended (target_node->reminder_misc_table,
					  misc_param,
					  NULL,
					   NULL)) 
		{
			if (reminder_list_misc_params_type [param_type] == G_TYPE_NONE)
				g_hash_table_insert (target_node->reminder_misc_table,
						     (gpointer) misc_param,
						     NULL);
			else
				g_hash_table_insert (target_node->reminder_misc_table,
						     (gpointer) misc_param,
						     reminder_list_clone_data (data, param_type));

		} else {
		g_warning ("Tried adding an already existant parameter in a reminder");

		return;
	}

	if (!g_list_find_custom (target_node->reminder_misc,
				 misc_param,
				 (GCompareFunc) g_strcmp0))
		{
			target_node->reminder_misc = g_list_insert (target_node->reminder_misc,
								    g_strdup (misc_param),
								    -1);
		}
}

void
reminder_list_misc_param_set (ReminderList *target_node,
			      ReminderListMiscParams param_type,
			      gpointer data)
{
	const gchar *misc_param;

	g_return_if_fail (target_node != NULL);

	if (param_type > NUM_OF_PARAMS) {
		g_warning ("Invalid parameter type specified to be added");
		return;
	}

	misc_param = reminder_list_misc_param [param_type];

	if (g_hash_table_lookup_extended (target_node->reminder_misc_table,
					  misc_param,
					  NULL,
					   NULL)) 
		{
			if (reminder_list_misc_params_type [param_type] != G_TYPE_NONE)
				g_hash_table_insert (target_node->reminder_misc_table,
						     (gpointer) misc_param,
						     reminder_list_clone_data (data, param_type));

		} else {
		g_warning ("Tried setting a non-existant parameter in a reminder");

		return;
	}
}

gboolean
reminder_list_misc_param_exist (ReminderList *target_node,
				ReminderListMiscParams param_type)
{
	gboolean ret = FALSE;
	const gchar *misc_param;

	g_return_val_if_fail (target_node != NULL,
			      ret);

	if (param_type > NUM_OF_PARAMS) {
		g_warning ("Invalid parameter type specified to be added");
		return ret;
	}

	misc_param = reminder_list_misc_param [param_type];

	if (g_hash_table_lookup_extended (target_node->reminder_misc_table,
					  misc_param,
					  NULL,
					  NULL))
		ret = TRUE;

	if (g_list_find_custom (target_node->reminder_misc,
				misc_param,
				(GCompareFunc) g_strcmp0))
		ret = TRUE;

	return ret;
}

GValue *
reminder_list_misc_param_get_val (ReminderList *target_node,
				  ReminderListMiscParams param_type)
{
	GValue *ret = NULL;
	gint param_val_type;

	if (param_type > NUM_OF_PARAMS) {
		g_warning ("Invalid parameter type specified to be added");
		return ret;
	}

	param_val_type = reminder_list_misc_params_type [param_type];

	if (!g_hash_table_lookup_extended (target_node->reminder_misc_table,
					   reminder_list_misc_param [param_type],
					   NULL,
					   NULL))
		return ret;

	ret = (GValue *) g_malloc0 (sizeof (GValue));

	if (param_val_type == G_TYPE_NONE);
	
	else if (param_val_type == G_TYPE_INT) {
		g_value_init (ret,
			      G_TYPE_INT);

		g_value_set_int (ret,
				 * (gint *) (g_hash_table_lookup (target_node->reminder_misc_table,
							reminder_list_misc_param [param_type])));
	} else if (param_val_type == G_TYPE_STRING) {
		g_value_init (ret, G_TYPE_STRING);

		g_value_set_string (ret,
				    (gchar *) g_hash_table_lookup (target_node->reminder_misc_table,
						      reminder_list_misc_param [param_type]));
	}

	return ret;
}

void
reminder_list_misc_param_delete (ReminderList *target_node,
				 ReminderListMiscParams param_type)
{
	const gchar *misc_param;
	GList *del_elem;

	g_return_if_fail (target_node != NULL);

	if (param_type > NUM_OF_PARAMS) {
		g_warning ("Invalid parameter type specified to be added");
		return;
	}

	misc_param = reminder_list_misc_param [param_type];

	if ((del_elem = g_list_find_custom (target_node->reminder_misc,
					    misc_param,
					    (GCompareFunc) g_strcmp0)))
		target_node->reminder_misc = g_list_remove (target_node->reminder_misc,
							    del_elem->data);
}

void
reminder_list_set_prio_det (ReminderList *target_node,
			    guint vis_alrm_type,
			    gboolean snd_alrm_play,
			    const gchar *script_path)
{
	g_return_if_fail (target_node != NULL);
	g_return_if_fail (target_node->reminder_prio == 6);

	target_node->prio_det->vis_alrm_type = vis_alrm_type;
	target_node->prio_det->snd_alrm_play = snd_alrm_play;

	if (script_path && strlen (script_path))
		target_node->prio_det->script_path = g_strdup (script_path);
	else
		target_node->prio_det->script_path = NULL;
}
