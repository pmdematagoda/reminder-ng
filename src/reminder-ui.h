/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009 - 2010 Pramod Dematagoda <pmd.lotr.gandalf@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib-object.h>

G_BEGIN_DECLS
#define REMINDER_TYPE_UI           (reminder_ui_get_type ())
#define REMINDER_UI(o)             (G_TYPE_CHECK_INSTANCE_CAST ((o), REMINDER_TYPE_UI, ReminderUI))
#define REMINDER_UI_CLASS(k)       (G_TYPE_CHECK_CLASS_CAST ((k), REMINDER_TYPE_UI, ReminderUIClass))
#define REMINDER_IS_UI(o)          (G_TYPE_CHECK_INSTANCE_TYPE ((o), REMINDER_TYPE_UI))
#define REMINDER_IS_UI_CLASS(k)    (G_TYPE_CHECK_CLASS_TYPE ((k), REMINDER_TYPE_UI))
#define REMINDER_UI_GET_CLASS(o)   (G_TYPE_INSTANCE_GET_CLASS ((k), REMINDER_TYPE_UI, ReminderUI))

typedef struct ReminderUIPrivate ReminderUIPrivate;

typedef struct
{
	GObject               parent;
	ReminderUIPrivate    *priv;
} ReminderUI;

typedef struct
{
	GObjectClass    parent_class;
} ReminderUIClass;

/* List of columns in the main tree view widgets */
enum
{
	PRIORITY_COL,
	TITLE_COL,
	DATE_COL,
	TIME_COL,
	EPOCH_COL,
	NO_OF_COL
};

GType         reminder_ui_get_type        (void);
ReminderUI   *reminder_ui_new             (void);
void          reminder_ui_attach_core     (ReminderUI *ui,
					   ReminderCore *core);

G_END_DECLS
